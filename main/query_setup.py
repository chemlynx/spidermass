# Python wrapper for the ChemSpider API.
# from ctypes import *
# requests for ChemSpider API.
try:
    import chemspiderREST
except:
    print('You are OFFLINE. ChemSpider REST requests will not work.')


import requests

try:
    tokenfile = open('chemspider-token.txt')
    ChemSpiderToken = tokenfile.readline().strip()
    tokenfile.close()

    TOKEN = ChemSpiderToken

except:
    print(
        """\n
    chemspiderREST error: No chemspider-token.txt file found.\n
    Please go to https://developer.rsc.org/ and register an account, you will be able
    to create your own key from the 'My Keys' page.\n Use the key to replace the value in a text file called chemspider-token.txt in the main directory.
    By default a key is restricted to 1000 requests per month, but it is possible to obtain an increased allowance.
    ONLINE REST REQUESTS TO CHEMSPIDER WILL NOT WORK WITHOUT TOKEN.\n""",
    )

    TOKEN = '0'


class base_mass:

    # sodium: bool, potassium: bool, ammonium: bool, proton: bool, hydride: bool, uncharged: bool):
    def __init__(self, mass: float, varience: float, m: int, m_plus_1: int, m_plus_2: int, m_plus_3: int, ion_species: list[str]):
        self.mass = mass
        self.varience = varience
        self.intensityM = m
        self.intensityMplus1 = m_plus_1
        self.intensityMplus2 = m_plus_2
        self.intensityMplus3 = m_plus_3
        self.ion_species = ion_species

    def __str__(self):
        return f"""input mass was: {self.mass}
        input varience was: {self.varience}
        This covers a range of {self.mass-self.varience} to {self.mass+self.varience}
        M = {self.intensityM}
        M+1 = {self.intensityMplus1}
        M+2 = {self.intensityMplus2}
        M+3 = {self.intensityMplus3}
        ions = {self.ion_species}"""  # sodium: {self.sodium}, potassium: {self.potassium}, ammonium: {self.ammonium}, proton: {self.proton}, hydride: {self.hydride}, uncharged: {self.uncharged}"""

    def construct_query_ions(self):
        # screen = [self.sodium, self.potassium, self.hydride, self.proton, self.uncharged, self.ammonium]
        # screened = [x for x in screen if x]
        # print(screened)
        calculated = [self.ion_calcs(x) for x in self.ion_species]
        return calculated

    def ion_calcs(self, ligand):
        ionisation_mode = f'[M+{ligand.label[:-1]}]{ligand.label[-1]}'
        print(f'ionisation mode = {ionisation_mode}')
        molecular_ion = self.mass - ligand.ligand_species_mass + \
            (ligand.charge * 0.000054857990)
        lower_bound_molecular_mass = molecular_ion - self.varience
        upper_bound_molecular_mass = molecular_ion + self.varience
        return (ionisation_mode, molecular_ion, lower_bound_molecular_mass, upper_bound_molecular_mass)


class compound_ion:
    label: str
    ligand_species_mass: float
    charge: int

    def __init__(self, label: str, ligand_mass: float, charge: str):
        self.label = label
        self.ligand_species_mass = ligand_mass
        self.charge = int(charge)

    def __repr__(self) -> str:
        return f"""label: {self.label}\nligand_species_mass: {self.ligand_species_mass}\ncharge: {self.charge}\n\n"""


def ion_instance(species, molecular_ion_mass, charge):
    return compound_ion(species, molecular_ion_mass, charge)


data = [
    ('Na+', 22.98976, '+1'),
    ('K+', 39.0983, '+1'),
    ('H+', 1.0079, '+1'),
    ('H-', 1.0079, '-1'),
    ('0', 0, '0'),
    ('NH4+', 18.039, '+1'),
]


molecular_ions = {x[0]: ion_instance(*x) for x in data}


class query:
    def __init__(self, name):
        self.name = name

    def enumerate_query_masses(self, base_mass, ionisation_types):
        query_ionisation_modes = base_mass.ion_species
        specified_ionisation_types = {
            k: v for k, v in ionisation_types.items() if k in query_ionisation_modes
        }
        # masses = specified_ionisation_types
        return specified_ionisation_types


class query_ion:
    def __init__(self, mass: float, varience: float, adduct_species: compound_ion):
        self.ionizationmode = f'[M+{adduct_species.label[:-1]}]{adduct_species.label[-1]}'
        self.varience = varience
        self.charge = adduct_species.charge
        self.median_mass = round(
            (
                mass - adduct_species.ligand_species_mass +
                (self.charge * 0.000054857990)
            ), 6,
        )
        self.uppermasslimitMZ = round((self.median_mass + self.varience), 6)
        self.lowermasslimitMZ = round((self.median_mass - self.varience), 6)

    def __repr__(self) -> str:
        return f"""\nionizationmode = {self.ionizationmode}\nvarience = {self.varience}\ncharge = {self.charge}\nmedian_mass = {self.median_mass}\nuppermasslimitMZ = {self.uppermasslimitMZ}\nlowermasslimitMZ = {self.lowermasslimitMZ}"""


headers = {
    'useragent': f'Spidermass_supertest',
    'apikey': TOKEN,
}


def runner(example):
    if example is None:
        example = base_mass(283.2135, 0.001, 50, 100, 6, 1, ['H+', 'Na+'])
    print(example)
    res = query('test')
    mes = res.enumerate_query_masses(example, molecular_ions)
    search_ions = [
        query_ion(
            example.mass, example.varience,
            mes[query_species],
        ) for query_species in mes
    ]
    useCScheck = True  # a fudge for now
    if useCScheck:
        SearchTimeOut = 5
        for ion in search_ions:
            placeHolder = chemspiderREST.newIntrinsicPropSeachMonoMWREST(
                ion, SearchTimeOut,
            )
            print(placeHolder)
    return search_ions
