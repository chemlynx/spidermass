#    SpiderMass is a software for the automated generation of chemical databases and the identification of compounds.
#    Copyright (C) 2013-2017 Dr. Robert Winkler
#    email: robert.winkler@cinvestav.mx, robert.winkler@bioprocess.org
#    CINVESTAV Unidad Irapuato
#    Km. 9.6 Libramiento Norte Carr. Irapuato-León
#    36821 Irapuato Gto., México
#    Tel.: +52-462-6239-635
#    http://www.ira.cinvestav.mx/lababi.aspx
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# FOR USING THIS LIBRARY YOU HAVE TO REGISTER AT CHEMSPIDER
# (FREE FOR ACADEMICS) AND PUT THE TOLKEN IN A FILE CALLED
# chemspider-token.txt in the main directory of SpiderMass
import json
import sys
import time

import requests
from query_setup import query_ion

try:
    tokenfile = open('chemspider-token.txt')
    ChemSpiderToken = tokenfile.readline().strip()
    tokenfile.close()

    TOKEN = ChemSpiderToken

except:
    print(
        """\n
    chemspiderREST error: No chemspider-token.txt file found.\n
    Please go to https://developer.rsc.org/ and register an account, you will be able
    to create your own key from the 'My Keys' page.\n Use the key to replace the value in a text file called chemspider-token.txt in the main directory.
    By default a key is restricted to 1000 requests per month, but it is possible to obtain an increased allowance.
    ONLINE REST REQUESTS TO CHEMSPIDER WILL NOT WORK WITHOUT TOKEN.\n""",
    )

    TOKEN = '0'

if '-' in TOKEN:
    print(
        """It looks like you are trying to use a ChemSpider SOAP token to access the
    Royal Society of Chemistry's RESTful APIs. The new APIs require a different key.
    Please go to https://developer.rsc.org/ and register an account, you will be able
    to create your own key from the 'My Keys' page.\n Use the key to replace the value in a text file called chemspider-token.txt in the main directory
    By default a key is restricted to 1000 requests per month, but it is possible to obtain an increase allowance.""",
    )

headers = {
    'useragent': f'TEST{3}',  # Spidermass_{sys.version}',
    'apikey': TOKEN,
}


def IntrinsicPropSeachMonoMWRESTUpperLower(MinMonoW, MaxMonoW, SearchTimeOut):
    """Wraps IntrinsicPropSeachMonoMWREST to provide legacy search function to input an Upper and Lower bound, rather than a mid-point and a tolerance"""
    tolerance = round((MaxMonoW - MinMonoW) * 0.5, 5)
    medianMass = round((MinMonoW + tolerance), 5)
    return IntrinsicPropSeachMonoMWREST(medianMass, tolerance, SearchTimeOut)


def IntrinsicPropSeachMonoMWREST(
    medianMass, tolerance, SearchTimeOut=3, headers=headers,
):

    print(
        f"""Searching for Monoisotopic Mass {medianMass} +/- {tolerance}\n
         in ChemSpider""",
    )
    print(
        f'Spidermass, Python{sys.version}, requestsVersion={requests.__version__}',
    )

    monoisotopicMassParameters = {
        'options': {'complexity': 'single', 'isotopic': ''},
        'orderBy': 'dataSourceCount',
        'orderDirection': 'descending',
        'monoisotopicMass': {'mass': medianMass, 'range': tolerance},
    }
    # print(monoisotopicMassParameters)

    CSIntrinsicPropSearchURL = (
        'https://api.rsc.org/compounds/v1/filter/intrinsicproperty'
    )

    response = requests.post(
        CSIntrinsicPropSearchURL, json=monoisotopicMassParameters, headers=headers,
    )
    time.sleep(1)
    SearchTimeOut = int(SearchTimeOut + 1)

    for i in range(1, SearchTimeOut):

        if response.status_code == 200:
            query_id_json = json.loads(response.text)
            query_id = query_id_json['queryId']
            print(query_id)
            status_url = f'https://api.rsc.org/compounds/v1/filter/{query_id}/status'

            status_response = requests.get(status_url, headers=headers)
            searchStatus = json.loads(status_response.text)
            searchStatus = searchStatus['status']

            print(f'Status of ChemSpider REST request: {searchStatus}')
        else:
            print(response.reason, response.status_code)

        if searchStatus == 'Processing':
            time.sleep(1)

        if searchStatus == 'Complete':
            # rsc-test-uat.apigee.net
            results_url = f'https://api.rsc.org/compounds/v1/filter/{query_id}/results'
            print(results_url)

            results_response = requests.get(results_url, headers=headers)
            searchResults = json.loads(results_response.text)
            resultCSint = searchResults['results']
            print('Obtaining results..')
            break

        if i == (SearchTimeOut - 1):
            resultCSint = ['empty']
            print(f'Timeout: >{SearchTimeOut - 1}s without result')
            break

        if searchStatus not in ('Complete', 'Processing'):
            resultCSint = ['empty']
            break

    print('REST request finished')

    return resultCSint


def IntrinsicPropSeachMonoMWRESTUpperLower(query_ion: query_ion, SearchTimeOut):
    """Wraps IntrinsicPropSeachMonoMWREST to provide legacy search function to input an Upper and Lower bound, rather than a mid-point and a tolerance"""
    tolerance = round((MaxMonoW - MinMonoW) * 0.5, 5)
    medianMass = round((MinMonoW + tolerance), 5)
    return IntrinsicPropSeachMonoMWREST(medianMass, tolerance, SearchTimeOut)


def newIntrinsicPropSeachMonoMWREST(
    query_ion: query_ion, SearchTimeOut, headers=headers,
):

    print(
        f"""Searching for Monoisotopic Mass {query_ion.median_mass} +/- {query_ion.varience}\n
         in ChemSpider""",
    )
    print(
        f'Spidermass, Python{sys.version}, requestsVersion={requests.__version__}',
    )

    monoisotopicMassParameters = {
        'options': {'complexity': 'single', 'isotopic': ''},
        'orderBy': 'dataSourceCount',
        'orderDirection': 'descending',
        'monoisotopicMass': {'mass': query_ion.median_mass, 'range': query_ion.varience},
    }
    print(monoisotopicMassParameters, headers)

    CSIntrinsicPropSearchURL = (
        'https://api.rsc.org/compounds/v1/filter/intrinsicproperty'
    )

    response = requests.post(
        CSIntrinsicPropSearchURL, json=monoisotopicMassParameters, headers=headers,
    )
    time.sleep(1)
    SearchTimeOut = int(SearchTimeOut + 1)

    for i in range(1, SearchTimeOut):

        if response.status_code == 200:
            query_id_json = json.loads(response.text)
            query_id = query_id_json['queryId']
            print(query_id)
            status_url = f'https://api.rsc.org/compounds/v1/filter/{query_id}/status'

            status_response = requests.get(status_url, headers=headers)
            searchStatus = json.loads(status_response.text)
            searchStatus = searchStatus['status']

            print(f'Status of ChemSpider REST request: {searchStatus}')
        else:
            print(response.reason, response.status_code)

        if searchStatus == 'Processing':
            time.sleep(1)

        if searchStatus == 'Complete':
            # rsc-test-uat.apigee.net
            results_url = f'https://api.rsc.org/compounds/v1/filter/{query_id}/results'
            print(results_url)

            results_response = requests.get(results_url, headers=headers)
            searchResults = json.loads(results_response.text)
            resultCSint = searchResults['results']
            print('Obtaining results..')
            break

        if i == (SearchTimeOut - 1):
            resultCSint = ['empty']
            print(f'Timeout: >{SearchTimeOut - 1}s without result')
            break

        if searchStatus not in ('Complete', 'Processing'):
            resultCSint = ['empty']
            break

    print('REST request finished')

    return resultCSint


def batchRecordsGet(listOfIds, headers=headers):
    import pprint as pp

    fields = [
        'SMILES',
        'Formula',
        'InChI',
        'InChIKey',
        'StdInChI',
        'StdInChIKey',
        'AverageMass',
        'MolecularWeight',
        'MonoisotopicMass',
        'NominalMass',
        'CommonName',
        'ReferenceCount',
        'DataSourceCount',
        'PubMedCount',
        'RSCCount',
        'Mol2D',
        'Mol3D',
    ]

    body = {'recordIds': listOfIds, 'fields': fields}
    batchRecordDetailsUrl = 'https://api.rsc.org/compounds/v1/records/batch'
    response = requests.post(
        batchRecordDetailsUrl, headers=headers, data=json.dumps(body),
    )
    time.sleep(1)
    print(
        listOfIds,
        response.status_code,
        response.reason,
        '\n',
        batchRecordDetailsUrl,
        body,
    )

    if response.status_code == 200:
        results_json = json.loads(response.text)
        resultCSintList = results_json['records']
    else:
        print(
            f'There was an error with your attempt to get Batch record data, the request failed with the following error:\n{response.status_code}: {response.reason}',
        )

    print('REST request finished')

    return resultCSintList
