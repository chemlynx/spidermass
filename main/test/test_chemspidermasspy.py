import chemspidermasspy

completeAspirinResult = {
    "csid": "2157",
    "_imageurl": None,
    "_mf": "C_{9}H_{8}O_{4}",
    "_smiles": "CC(OC1=C(C(=O)O)C=CC=C1)=O\r\nCC(=O)OC1C=CC=CC=1C(O)=O",
    "_inchi": "InChI=1/C9H8O4/c1-6(10)13-8-5-3-2-4-7(8)9(11)12/h2-5H,1H3,(H,11,12)",
    "_inchikey": "BSYNRYMUTXBXSQ-UHFFFAOYAW",
    "_averagemass": 180.1574,
    "_molecularweight": 180.1574,
    "_monoisotopicmass": 180.042252,
    "_nominalmass": 180.0,
    "_stdinchi": "InChI=1S/C9H8O4/c1-6(10)13-8-5-3-2-4-7(8)9(11)12/h2-5H,1H3,(H,11,12)",
    "_stdinchikey": "BSYNRYMUTXBXSQ-UHFFFAOYSA-N",
    "_commonname": "Aspirin",
    "_image": None,
    "_mol": "\n  Ketcher 08291811042D 1   1.00000     0.00000     0\n\n 13 13  0     0  0            999 V2000\n    4.4977   -3.4633    0.0000 C   0  0  0  0  0  0  0        0  0  0\n    3.9985   -2.5997    0.0000 C   0  0  0  0  0  0  0        0  0  0\n    4.4977   -1.7317    0.0000 O   0  0  0  0  0  0  0        0  0  0\n    3.0000   -2.5997    0.0000 O   0  0  0  0  0  0  0        0  0  0\n    2.5008   -1.7317    0.0000 C   0  0  0  0  0  0  0        0  0  0\n    3.0000   -0.8680    0.0000 C   0  0  0  0  0  0  0        0  0  0\n    2.5008    0.0000    0.0000 C   0  0  0  0  0  0  0        0  0  0\n    1.4977    0.0000    0.0000 C   0  0  0  0  0  0  0        0  0  0\n    0.9985   -0.8680    0.0000 C   0  0  0  0  0  0  0        0  0  0\n    1.4977   -1.7317    0.0000 C   0  0  0  0  0  0  0        0  0  0\n    0.9985   -2.5997    0.0000 C   0  0  0  0  0  0  0        0  0  0\n    0.0000   -2.5997    0.0000 O   0  0  0  0  0  0  0        0  0  0\n    1.4977   -3.4633    0.0000 O   0  0  0  0  0  0  0        0  0  0\n  1  2  1  0     0  0\n  2  3  2  0     0  0\n  2  4  1  0     0  0\n  4  5  1  0     0  0\n  5  6  1  0     0  0\n  5 10  2  0     0  0\n  6  7  2  0     0  0\n  7  8  1  0     0  0\n  8  9  2  0     0  0\n  9 10  1  0     0  0\n 10 11  1  0     0  0\n 11 12  1  0     0  0\n 11 13  2  0     0  0\nM  END\n",
    "_mol3D": "2244\n  Marvin  12300703363D          \n\n 21 21  0  0  0  0            999 V2000\n   -0.8612   -1.2262   -0.2662 O   0  0  0  0  0  0  0  0  0  0  0  0\n    3.0617   -1.6852    0.1961 O   0  0  0  0  0  0  0  0  0  0  0  0\n    1.4447   -2.5635   -0.6900 O   0  0  0  0  0  0  0  0  0  0  0  0\n   -2.4189   -2.5903   -0.0729 O   0  0  0  0  0  0  0  0  0  0  0  0\n   -0.1412   -0.1785   -0.4132 C   0  0  0  0  0  0  0  0  0  0  0  0\n    1.2784   -0.2956   -0.3433 C   0  0  0  0  0  0  0  0  0  0  0  0\n   -0.7302    1.0925   -0.6232 C   0  0  0  0  0  0  0  0  0  0  0  0\n    2.0415    0.9020   -0.3420 C   0  0  0  0  0  0  0  0  0  0  0  0\n    0.0491    2.2590   -0.6408 C   0  0  0  0  0  0  0  0  0  0  0  0\n    1.4378    2.1648   -0.4724 C   0  0  0  0  0  0  0  0  0  0  0  0\n    1.9369   -1.5271   -0.2701 C   0  0  0  0  0  0  0  0  0  0  0  0\n   -1.9700   -1.4955    0.2325 C   0  0  0  0  0  0  0  0  0  0  0  0\n   -2.7371   -0.6848    1.1595 C   0  0  0  0  0  0  0  0  0  0  0  0\n   -1.7396    1.1850   -0.7587 H   0  0  0  0  0  0  0  0  0  0  0  0\n    3.0610    0.8861   -0.2532 H   0  0  0  0  0  0  0  0  0  0  0  0\n   -0.3946    3.1715   -0.7684 H   0  0  0  0  0  0  0  0  0  0  0  0\n    2.0064    3.0149   -0.4552 H   0  0  0  0  0  0  0  0  0  0  0  0\n   -3.0283   -1.2693    2.0341 H   0  0  0  0  0  0  0  0  0  0  0  0\n   -3.6371   -0.3138    0.6664 H   0  0  0  0  0  0  0  0  0  0  0  0\n   -2.1731    0.1707    1.5341 H   0  0  0  0  0  0  0  0  0  0  0  0\n    3.5136   -1.0170    0.5467 H   0  0  0  0  0  0  0  0  0  0  0  0\n  1  5  1  0  0  0  0\n  1 12  1  0  0  0  0\n  2 11  1  0  0  0  0\n  2 21  1  0  0  0  0\n  3 11  2  0  0  0  0\n  4 12  2  0  0  0  0\n  5  6  1  0  0  0  0\n  5  7  2  0  0  0  0\n  6  8  2  0  0  0  0\n  6 11  1  0  0  0  0\n  7  9  1  0  0  0  0\n  7 14  1  0  0  0  0\n  8 10  1  0  0  0  0\n  8 15  1  0  0  0  0\n  9 10  2  0  0  0  0\n  9 16  1  0  0  0  0\n 10 17  1  0  0  0  0\n 12 13  1  0  0  0  0\n 13 18  1  0  0  0  0\n 13 19  1  0  0  0  0\n 13 20  1  0  0  0  0\nM  END\n> <PUBCHEM_BONDANNOTATIONS>\n5  6  8\n5  7  8\n6  8  8\n7  9  8\n8  10  8\n9  10  8\n\n$$$$\n",
}


def test_compoundObjectCreation():
    results = chemspidermasspy.Compound(236)
    return results.__dict__


compoundObject = test_compoundObjectCreation()

assert len(compoundObject) == 16
assert compoundObject["csid"] == "236"


def test_compound_mf():
    results = chemspidermasspy.Compound(2157)
    results.loadextendedcompoundinfo()
    return results.__dict__


testrun = test_compound_mf()
assert testrun == completeAspirinResult


def test_image_url():
    """Test image_url returns a valid URL."""
    cmpdObject = chemspidermasspy.Compound(730)
    url = cmpdObject.imageurl
    assert "http://www.chemspider.com/ImagesHandler.ashx?id=" in url


def test_molecular_formula():
    """Test Compound property molecular_formula."""
    cmpdObject = chemspidermasspy.Compound(730)
    assert cmpdObject.mf == "C_{2}H_{5}NO_{2}"
    # Ensure value is the same on subsequent access from cache
    assert cmpdObject.mf == "C_{2}H_{5}NO_{2}"


def test_smiles():
    """Test Compound property smiles."""
    cmpdObject = chemspidermasspy.Compound(730)
    assert cmpdObject.smiles == "C(C(=O)O)N"
    # Ensure value is the same on subsequent access from cache
    assert cmpdObject.smiles == "C(C(=O)O)N"


def test_inchi():
    """Test Compound property inchi."""
    cmpdObject = chemspidermasspy.Compound(730)
    assert cmpdObject.inchi == "InChI=1/C2H5NO2/c3-1-2(4)5/h1,3H2,(H,4,5)"
    # Ensure value is the same on subsequent access from cache
    assert cmpdObject.inchi == "InChI=1/C2H5NO2/c3-1-2(4)5/h1,3H2,(H,4,5)"


def test_stdinchi():
    """Test Compound property stdinchi."""
    cmpdObject = chemspidermasspy.Compound(730)
    assert cmpdObject.stdinchi == "InChI=1S/C2H5NO2/c3-1-2(4)5/h1,3H2,(H,4,5)"
    # Ensure value is the same on subsequent access from cache
    assert cmpdObject.stdinchi == "InChI=1S/C2H5NO2/c3-1-2(4)5/h1,3H2,(H,4,5)"


def test_inchikey():
    """Test Compound property inchikey."""
    cmpdObject = chemspidermasspy.Compound(730)
    assert cmpdObject.inchikey == "DHMQDGOQFOQNFH-UHFFFAOYAW"
    # Ensure value is the same on subsequent access from cache
    assert cmpdObject.inchikey == "DHMQDGOQFOQNFH-UHFFFAOYAW"


def test_stdinchikey():
    """Test Compound property stdinchikey."""
    cmpdObject = chemspidermasspy.Compound(730)
    assert cmpdObject.stdinchikey == "DHMQDGOQFOQNFH-UHFFFAOYSA-N"
    # Ensure value is the same on subsequent access from cache
    assert cmpdObject.stdinchikey == "DHMQDGOQFOQNFH-UHFFFAOYSA-N"


def test_masses():
    """Test Compound property average_mass, molecular_weight, monoisotopic_mass, nominal_mass."""
    cmpdObject = chemspidermasspy.Compound(730)
    assert cmpdObject.averagemass == 75.0666
    assert cmpdObject.molecularweight == 75.0666
    assert cmpdObject.monoisotopicmass == 75.032028


def test_name():
    """Test Compound property common_name."""
    cmpdObject = chemspidermasspy.Compound(730)
    assert cmpdObject.commonname == "Glycine"


def test_molfiles1():
    """Test Compound property mol2d, mol3d"""
    cmpdObject = chemspidermasspy.Compound(730)
    assert "V2000" in cmpdObject.mol
    assert "V2000" in cmpdObject.mol3D


def test_molfiles2():
    """Test Compound property mol2d, mol3d"""
    cmpdObject = chemspidermasspy.Compound(236)
    assert "V2000" in cmpdObject.mol
    assert "V2000" in cmpdObject.mol3D
