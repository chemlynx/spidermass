#    SpiderMass is a software for the automated generation of chemical databases and the identification of compounds.

#    Copyright (C) 2013-2017 Dr. Robert Winkler
#    email: robert.winkler@cinvestav.mx, robert.winkler@bioprocess.org
#    CINVESTAV Unidad Irapuato
#    Km. 9.6 Libramiento Norte Carr. Irapuato-León
#    36821 Irapuato Gto., México
#    Tel.: +52-462-6239-635
#    http://www.ira.cinvestav.mx/lababi.aspx

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.

#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

# String processing
import re

# from time import sleep
# from time import strftime
import tkinter.messagebox as tkMessageBox

# Python URL
from tkinter import Button, Checkbutton, Frame, IntVar, Menu, Tk, W
from tkinter.filedialog import askopenfilename

# Python wrapper for the ChemSpider API.
import chemspidermasspy

# Definition of functions


def help_usage():
    tkMessageBox.showinfo("Help", "Read the README file \nJust try, it's easy!")


def help_about():
    tkMessageBox.showinfo(
        "About",
        """SpiderMass Database Generator 1.1\n
        License: GPLv3 \nRobert Winkler \n
        CINVESTAV Irapuato, Mexico, 2013\n
        http://www.ira.cinvestav.mx/lababi.aspx""",
    )


def DB_ready():
    tkMessageBox.showinfo(
        "DB Generation",
        """The SpiderMass database has been generated.\n
        Look for the files new_SpiderMassDB.csv and dbgenerator_not_found.txt.\n
        Please do not forget to rename your new DB!""",
    )


def DB_processing():
    tkMessageBox.showinfo(
        "DB Generation",
        """The SpiderMass database generation will start after you click OK.\n
        Depending on the length of your list, the generation may take a while; Please be patient!\n
        You can monitor the progress of the ChemSpider DB search on the console window.""",
    )


def Quit_SpiderMass():
    quit()


# Definition of global variables

# Main program


def DB_Generator():
    """Intiatates opening of a file containing a list of names and then uses the RSC APIs
    to try to look up the CSID, Mass and Molecular formula, these are output to a csv file
    and that are not matched are stored in a separate text file"""

    filename = askopenfilename()
    DB_processing()
    # Write DB
    SpiderMassDBfile = open("new_SpiderMassDB.csv", "w")
    notfoundfile = open("dbgenerator_not_found.txt", "w")

    if mzminecheck.get() == 1:
        headline = "ID;m/z;Retention time (min);Identity;Formula\n"
    if mzminecheck.get() == 0:
        headline = "Search_Term;ChemSpiderID;Common_Name;Formula;Monoisotopic_Mass\n"
    SpiderMassDBfile.write(headline)

    with open(filename) as inputFile:
        namesList = inputFile.readlines()

    namesList = [name.rstrip("\n").strip() for name in namesList]
    for linenumber, chemName in enumerate(namesList, start=1):
        print(f"{linenumber} Searching ChemspiderDB for: {chemName}")

        try:
            chemspiderCompoundID = chemspidermasspy.find_one(chemName)

            if chemspiderCompoundID:

                chemName = re.sub(
                    ";", ",", chemName
                )  # Not sure what the purpose of this line is

                chemspiderCompoundIDString = chemspiderCompoundID.csid
                chemspiderName = chemspiderCompoundID.commonname
                chemspiderNameString = str(chemspiderName)
                chemspiderNameString = re.sub(";", ",", chemspiderNameString)
                chemspiderNameString = re.sub("\n", " ", chemspiderNameString)

                chemspiderSumFormula = chemspiderCompoundID.mf
                chemspiderSumFormulaString = str(chemspiderSumFormula)
                chemspiderSumFormulaStringStrip = re.sub(
                    "[^a-zA-Z0-9\n\.]", "", chemspiderSumFormulaString
                )

                if mzminecheck.get() == 0:
                    chemspiderMonoMassString = str(
                        chemspiderCompoundID.monoisotopicmass
                    )
                    saveString = f"{chemName};{chemspiderCompoundIDString};{chemspiderNameString};{chemspiderSumFormulaStringStrip};{chemspiderMonoMassString}\n"

                    if chemspiderCompoundID.monoisotopicmass > 1:
                        SpiderMassDBfile.write(saveString)

                if mzminecheck.get() == 1:

                    chemspiderMonoMass = chemspiderCompoundID.monoisotopicmass
                    # for MZmine: protonized!
                    chemspiderMonoMass = chemspiderMonoMass + 1.0078250321

                    saveString = f"{linenumber};{chemspiderMonoMass};0;{chemspiderNameString};{chemspiderSumFormulaStringStrip}\n"
                    if chemspiderMonoMass > 1:
                        SpiderMassDBfile.write(saveString)

            else:
                notfoundfile.write(f"{chemName} not found in ChemSpider DB\n")

        except:
            notfoundfile.write(f"{chemName} caused exception error!\n")

    print(f"\n{linenumber} Compounds were searched in the ChemSpider DB\n")
    SpiderMassDBfile.close()
    notfoundfile.close()
    DB_ready()


# Main Window

root = Tk()
root.title("SpiderMass DB Generator 1.1")

# GUI

features = Frame(root)
features.grid(sticky=W)

# INPUT area

# Action Button

action_button = Button(
    features,
    text="Generate SpiderMass Database from compound list",
    command=DB_Generator,
)
action_button.grid(row=4, column=1)

mzminecheck = IntVar()
mzminecheckbox = Checkbutton(
    features,
    text="Generate MZmine custom DB (H+ ions, Not SpiderMass compatible!)",
    variable=mzminecheck,
    onvalue=1,
    offvalue=0,
    height=2,
)
mzminecheckbox.grid(row=6, column=1, sticky=W)
mzminecheckbox.deselect()

action_button = Button(features, text="Quit", command=Quit_SpiderMass)
action_button.grid(row=4, column=2)

# OUTPUT area

# creating a menu

menu = Menu(root)
root.config(menu=menu)

filemenu = Menu(menu)
menu.add_cascade(label="File", menu=filemenu)
filemenu.add_command(label="Exit", command=Quit_SpiderMass)

helpmenu = Menu(menu)
menu.add_cascade(label="Help", menu=helpmenu)
helpmenu.add_command(label="Usage", command=help_usage)
helpmenu.add_command(label="About...", command=help_about)

root.mainloop()
