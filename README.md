# README

SpiderMass is a collection of tools for the efficient identification of metabolites. The
project started as re-implementation of the Seven Golden Rules Software (7GR, Kind &
Fiehn http://fiehnlab.ucdavis.edu/projects/Seven_Golden_Rules/), but several functions
were added. Our database creator converts plain text lists with compound names etc. to
project-specific chemical databases.

## NEWS version 1.1

- The formula generator was updated and now also builds mixed formula, consisting of
  natural and stable isotope atoms (e.g. containing H/D).
- New **Windows Installer**. Tested on Windows 10, 64 bit.

## Windows Users ATTENTION

On Windws, the **SpiderMass default directory is 'USER/App/Roaming'**. This is where you
also should place a file named **'chemspider-token.txt'** with your RSC chemspider token
(free for Academics from <http://www.chemspider.com/>) and where you find your
**results**.

### Requirements

- Python 3
- OSA library
- Chemspider token
- It might be necessary to re-compile the C/C++ programs on your platform

### Contribution guidelines

Comments are welcome at any time!

### Who do I talk to?

robert.winkler@bioprocess.org; robert.winkler@cinvestav.mx
