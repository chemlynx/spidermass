#    SpiderMass is a software for the automated generation of chemical databases and the identification of compounds.
#    Copyright (C) 2013-2017 Dr. Robert Winkler
#    email: robert.winkler@cinvestav.mx, robert.winkler@bioprocess.org
#    CINVESTAV Unidad Irapuato
#    Km. 9.6 Libramiento Norte Carr. Irapuato-León
#    36821 Irapuato Gto., México
#    Tel.: +52-462-6239-635
#    http://www.ira.cinvestav.mx/lababi.aspx
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
# Python specific imports
import csv
import math
import re
import sys
import time
from subprocess import call
from typing import Dict
from typing import List
from typing import Tuple
from typing import Union

import numpy as np
from numpy import float64

# requests for ChemSpider API.
try:
    import chemspiderREST
except:
    print('You are OFFLINE. ChemSpider REST requests will not work.')


def load_mass_data() -> Tuple[float64, float64, float64, float64, float64, float64]:
    """Loads masses from NIST file and exposes a set of masses needed for
    calculations on Molecular Ion Species

    Returns:
        Tuple[float64, float64, float64, float64, float64, float64]: Provides all of the NIST masses for electron, proton, sodium, potassium, nitrogen and ammonium
    """

    isotopesdb = './isotopesdb/NIST.csv'
    isoDB = np.genfromtxt(
        isotopesdb, delimiter=';', names=True, dtype=None, encoding='utf-8',
    )
    numrows_isoDB = len(isoDB)
    E_isoDB = isoDB['element_E']
    RAM_isoDB = isoDB['relative_atomic_mass']

    MMW_H = 0
    MMW_Na = 0
    MMW_K = 0
    MMW_N = 0

    for j in range(numrows_isoDB):
        if E_isoDB[j] == 'e':
            MMW_e = int(RAM_isoDB[j])
        if E_isoDB[j] == 'H':
            MMW_H = int(RAM_isoDB[j])
        if E_isoDB[j] == 'Na':
            MMW_Na = int(RAM_isoDB[j])
        if E_isoDB[j] == 'K':
            MMW_K = int(RAM_isoDB[j])
        if E_isoDB[j] == 'N':
            MMW_N = int(RAM_isoDB[j])
    MMW_NH4 = MMW_N + (4 * MMW_H)
    print(
        f"""Electron weight: {MMW_e}
        H weight: {MMW_H}
        Na weight: {MMW_Na}
        K weight: {MMW_K}
        N weight: {MMW_N}
        NH4 weight: {MMW_NH4}""",
    )
    return MMW_e, MMW_H, MMW_Na, MMW_K, MMW_N, MMW_NH4


MMW_e, MMW_H, MMW_Na, MMW_K, MMW_N, MMW_NH4 = load_mass_data()


# Definition of functions


def IDF_checker(Formula, ionizationmode):
    """[summary]

    Args:
        Formula ([type]): [description]
        ionizationmode ([type]): [description]
    """
    # Special SpiderMass Feature: The Formula for the MS ION is calculated, and subsequently its isotope pattern
    print('\nIonization mode: ', ionizationmode)
    print('Isotope Distribution for Formula ', Formula)
    Splitter = re.findall(r'\d+|\D+', Formula)
    print('Formula Splitter : ', Splitter)

    if ionizationmode == '[M+H]+':
        try:
            Hindex = Splitter.index('H')
            print('H index: ', Hindex, '\n')
            HindexCount = Hindex + 1
            Splitter[HindexCount] = 1 + int(Splitter[HindexCount])
        except:
            Splitter.append('H')

    if ionizationmode == '[M+K]+':
        try:
            Kindex = Splitter.index('K')
            # print("K index: ", Kindex, "\n")
            KindexCount = Kindex + 1
            Splitter[KindexCount] = 1 + int(Splitter[KindexCount])
        except:
            Splitter.append('K')

    if ionizationmode == '[M+Na]+':
        try:
            Naindex = Splitter.index('Na')
            # print("Na index: ", Naindex, "\n")
            NaindexCount = Naindex + 1
            Splitter[NaindexCount] = 1 + int(Splitter[NaindexCount])
        except:
            Splitter.append('Na')

    if ionizationmode == '[M+NH4]+':
        try:
            Nindex = Splitter.index('N')
            # print("N index: ", Nindex, "\n")
            NindexCount = Nindex + 1
            Splitter[NindexCount] = 1 + int(Splitter[NindexCount])
        except:
            Splitter.append('N')
        try:
            Hindex = Splitter.index('H')
            # print("H index: ", Hindex, "\n")
            HindexCount = Hindex + 1
            Splitter[HindexCount] = 4 + int(Splitter[HindexCount])
        except:
            Splitter.append('H')
            Splitter.append('4')

    if ionizationmode == '[M-H]-':
        try:
            Hindex = Splitter.index('H')
            # print("H index: ", Hindex, "\n")
            HindexCount = Hindex + 1
            Splitter[HindexCount] = int(Splitter[HindexCount]) - 1
        except:
            print(
                """No H in putative formula:
                This is not a possible for ionization mode [M-H]-!\n""",
            )

    # print("Formula Splitter modified: ", Splitter)
    Formula = ''.join(map(str, Splitter))
    print('Isotope Distribution calculated for Formula of ION: ', Formula, '\n')

    # Call external program, depending on operating system

    if sys.platform == 'linux' or sys.platform == 'linux2':
        call(['./smisotope-linux', '-f', Formula])

    if sys.platform == 'win32':
        call(['smisotope-win.exe', '-f', Formula])

    isoDB = np.genfromtxt(
        'isotopes.csv', delimiter=';', names=True, dtype=None, encoding='utf-8',
    )

    """ isoDBfile = "isotopes.csv"  # Hardcoded for now - but should come from TK object
    with open(isoDBfile, "r", encoding="utf-8") as file:
        entries = list(csv.DictReader(file, dialect="excel", delimiter=";"))

    isoDB = dict(entries[0])
    print(isoDB)
 """
    TM0 = isoDB['TM0']
    TM1 = isoDB['TM1']
    TM2 = isoDB['TM2']
    TM3 = isoDB['TM3']

    TMarray = np.array([TM0, TM1, TM2, TM3])
    TMarray = TMarray / TMarray.max()  # normalize the vector
    print('Theoretical Isotope M  : ', TM0)
    print('Theoretical Isotope M+1: ', TM1)
    print('Theoretical Isotope M+2: ', TM2)
    print('Theoretical Isotope M+3: ', TM3, '\n')
    print('Theoretical Isotopes (normalized): ', TMarray)
    EMarray = np.array(
        [
            queryParameters['EM0'],
            queryParameters['EM1'],
            queryParameters['EM2'],
            queryParameters['EM3'],
        ],
    )
    EMarray = EMarray / EMarray.max()  # normalize the vector
    print('Experimental Isotopes (normalized): ', EMarray)
    IDF = EMarray - TMarray
    # print(IDF)
    IDF = np.abs(IDF)
    # print(IDF)
    IDF = np.sum(IDF)
    print('Isotope Distribution Fit (IDF): ', IDF, '\n')
    return IDF


def start_dbgenerator():
    import spidermass_dbgenerator

    spidermass_dbgenerator()


def set_parameters() -> dict[str, bool, int]:
    """Grab all the required parameters from the global variables set by Tkinter

    Returns:
        dict: A dictionary of parameters
    """

    parameters = []
    # To separate out
    parameters['CStimeout'] = 5
    # CStimeout = CStimeout_set.get()
    #

    parameters['MZpeaklist'] = MZpeaklistfilename_value.get()
    parameters['tolerance'] = float(tolerance_entry.get())
    parameters['hydrogencheck'] = hydrogencheck.get()
    parameters['useSMDBcheck'] = useSMDBcheck.get()
    parameters['useDNcheck'] = useDNcheck.get()
    parameters['sodiumcheck'] = sodiumcheck.get()
    parameters['minusHcheck'] = minusHcheck.get()
    parameters['useCScheck'] = useCScheck.get()
    parameters['useIDFcheck'] = useIDFcheck.get()
    parameters['potassiumcheck'] = potassiumcheck.get()
    parameters['ammoniumcheck'] = ammoniumcheck.get()
    parameters['zerocheck'] = zerocheck.get()
    parameters['minusHcheck'] = minusHcheck.get()
    parameters['query_peak_mass'] = float(monopeakvalue_entry.get())
    parameters['switchC'] = switchC.get()
    parameters['switch13C'] = switch13C.get()
    parameters['switchH'] = switchH.get()
    parameters['switchD'] = switchD.get()
    parameters['switchN'] = switchN.get()
    parameters['switch15N'] = switch15N.get()
    parameters['switch0'] = switchO.get()
    parameters['switchF'] = switchF.get()
    parameters['switchNa'] = switchNa.get()
    parameters['switchSi'] = switchSi.get()
    parameters['switchP'] = switchP.get()
    parameters['switchS'] = switchS.get()
    parameters['switchCl'] = switchCl.get()
    parameters['switchBr'] = switchBr.get()
    parameters['CSmaxHits'] = CSmaxHits_set.get()
    parameters['EM0'] = isoabundance1_set.get()
    parameters['EM1'] = isoabundance2_set.get()
    parameters['EM2'] = isoabundance3_set.get()
    parameters['EM3'] = isoabundance4_set.get()

    return parameters


queryParameters = {
    'CStimeout': 1,
    'MZpeaklist': True,
    'query_peak_mass': 1.00,
    'tolerance': 0.00,
    'useIDFcheck': False,
    'useSMDBcheck': True,
    'useDNcheck': False,
    'useCScheck': False,
    'CSmaxHits': 5,
    'EM0': 100,
    'EM1': 13,
    'EM2': 66,
    'EM3': 8,
}

ionSpecies = [
    {
        'label': 'hydrogencheck',
        'status': True,
        'species': '[M+H]+',
        'modifiers': [MMW_e, MMW_H],
    },
    {
        'label': 'sodiumcheck',
        'status': True,
        'species': '[M+Na]+',
        'modifiers': [MMW_e, MMW_Na],
    },
    {
        'label': 'potassiumcheck',
        'status': False,
        'species': '[M+k]+',
        'modifiers': [MMW_e, MMW_K],
    },
    {
        'label': 'ammoniumcheck',
        'status': False,
        'species': '[M+NH4]',
        'modifiers': [MMW_e, MMW_NH4],
    },
    {
        'label': 'zerocheck', 'status': False,
        'species': '+/- 0', 'modifiers': [0, 0],
    },
    {
        'label': 'minusHcheck',
        'status': False,
        'species': '[M-H]-',
        'modifiers': [(-1 * MMW_e), (-1 * MMW_H)],
    },
]

defaultElementOptions = {
    'switchC': True,
    'switch13C': False,
    'switchH': True,
    'switchD': False,
    'switchN': True,
    'switch15N': False,
    'switchO': True,
    'switchF': False,
    'switchNa': False,
    'switchSi': False,
    'switchP': False,
    'switchS': False,
    'switchCl': True,
    'switchBr': False,
}

#  lowermasslimitMZ = query_peak_mass - tolerance - MMW_H + MMW_e
#  uppermasslimitMZ = query_peak_mass + tolerance - MMW_H + MMW_e


def set_ion_species(
    ionSpecies: List[Dict[str, Union[str, bool, List[int]]]],
    query_peak_mass: float,
    tolerance: float,
) -> List[Dict[str, Union[str, bool, List[float64], float, List[int]]]]:
    """Parse the ionSpecies dict and trim to only the checked/selected ions and calculate molecular mass based on those ion adducts.

    Args:
        ionSpecies (List[Dict[str, Union[str, bool, List[int]]]]): [description]
        query_peak_mass (float): [description]
        tolerance (float): [description]

    Returns:
        List[Dict[str, Union[str, bool, List[float64], float, List[int]]]]: [description]
    """
    ionSpecies = [entry for entry in ionSpecies if entry['status']]

    for species in ionSpecies:
        species['corrected_parent_mass'] = (
            float(query_peak_mass)
            + species['modifiers'][0] - species['modifiers'][1]
        )
        species['tolerance'] = tolerance
    return ionSpecies


class queryParamObj:
    """Build an object to hold all of the interface inputs"""

    def __init__(self, name):
        self.name = name


def ion_search(
    # query_peak_mass: float,
    # tolerance: float,
    # ionSpecies: List[str],
    # parameter_dict=queryParameters,
    # elementOptions=defaultElementOptions,
):
    """Takes the mass of an ion, together with an allowed variance (tolerance)


    Args:
        query_peak_mass (float): Mass of ion
        tolerance (float): Allowed variance/error
        ionSpecies ([type]): [description]
        parameter_dict ([type], optional): [description]. Defaults to queryParameters.
        elementOptions ([type], optional): [description]. Defaults to defaultElementOptions.

    Returns:
        [type]: [description]
    """
    # parameter_dict["query_peak_mass"] = query_peak_mass
    # parameter_dict["tolerance"] = tolerance
    # ionSpecies = set_ion_species(ionSpecies, query_peak_mass, tolerance)

    """if parameter_dict["useSMDBcheck"]:
        print(f"{['useSMDBcheck']}")
        search_spidermassDB(query_peak_mass, tolerance, ionizationmode, queryParameters)

    if parameter_dict["useDNcheck"]:
        pass

    if parameter_dict["useIDFcheck"]:
        IDFvalue = IDF_checker(Formula, ionizationmode)
        SpiderHit = math.pow(IDFvalue * masserror, 2)
        IDFvalue = str("%f" % IDFvalue)
        SpiderHit = str("%.12f" % SpiderHit)

    if parameter_dict["useCScheck"]:
        pass

    for item in ionSpecies:
        pp.pprint(item)

    ## so far as I can tell IDFcheck is optionally called after seraching a spidermass DB and after the DeNovo generation - from within these functions - I want to pull it out and run it once

    if parameter_dict["useCScheck"]:
        max_compounds = (
            sum([species["status"] for species in ionSpecies])
            * parameter_dict["CSmaxHits"]
        )
        print(f"max_compounds = {max_compounds}")"""

    return 'fish'  # ionSpecies

    # parent_molecule_monopeakvalue_search = query_peak_mass + MMW_e - is this used at all?
    for species in ionSpecies:
        if parameter_dict['useSMDBcheck']:
            search_spidermassDB(
                SMresultfile,
                query_peak_mass,
                tolerance,
                ionizationmode,
            )

    if parameter_dict['useCScheck']:
        RESTrequest = chemspiderREST.IntrinsicPropSeachMonoMWREST(
            query_peak_mass,
            tolerance,
            CStimeout,
        )
        chemspider_decoder(
            RESTrequest,
            SMresultfile,
            query_peak_mass,
            tolerance,
            ionizationmode,
            query_peak_mass,
        )
    if parameter_dict['useDNcheck']:
        neutralmass = query_peak_mass
        de_novo_generator(
            SMresultfile,
            query_peak_mass,
            ionizationmode,
            neutralmass,
            tolerance,
        )

    SMresultfile = open('SpiderMassResults_dash_test.csv', 'w')
    SMresultwrite = str(
        'm/z'
        + ' +/- '
        + 'Mass Tolerance'
        + ';'
        + 'Ionization Mode'
        + ';'
        + 'Database'
        + ';'
        + 'Name'
        + ';'
        + 'ChemSpider ID'
        + ';'
        + 'Common Name (ChemSpider)'
        + ';'
        + 'Formula'
        + ';'
        + 'Monoisotopic Mass [Da]'
        + ';'
        + 'Mass Error [mDa]'
        + ';'
        + 'Isotope Distribution Fit (IDF)'
        + ';'
        + 'Spider Hit'
        + '\n',
    )
    SMresultfile.write(SMresultwrite)
    SMresultfile.close()
    tkMessageBox.showinfo(
        'SpiderMass DB search', 'You can find the results in SpiderMassResults.csv',
    )


def chemspider_decoder(
    RESTrequestInt,
    SMresultfile,
    query_peak_mass,
    tolerance,
    ionizationmode,
    parent_molecule_monopeakvalue_search,
):
    """[summary]

    Args:
        RESTrequestInt ([type]): [description]
        SMresultfile ([type]): [description]
        query_peak_mass ([type]): [description]
        tolerance ([type]): [description]
        ionizationmode ([type]): [description]
        parent_molecule_monopeakvalue_search ([type]): [description]
    """
    import chemspidermasspy

    print(f'RESTrequestInt: {RESTrequestInt}')

    if RESTrequestInt == ['empty']:
        print('No result from ChemSpider DB search!')

    else:
        try:
            HitNumber = len(RESTrequestInt)

            print(
                f'{HitNumber} results found in ChemSpider DB, importing the first {CSmaxHits} hits',
            )
            RESTrequestInt = RESTrequestInt[:CSmaxHits]
            print(f'sliced RESTrequestInt: {RESTrequestInt}')
            for CSID in RESTrequestInt:
                CSID = str(CSID)
                print(CSID)

                chemspiderCompoundID = chemspidermasspy.Compound(CSID)
                print('generated compound')

                chemspiderName = chemspiderCompoundID.commonname
                print(f'cs Name = {chemspiderName}, {type(chemspiderName)}')
                chemspiderNameString = str(chemspiderName)
                chemspiderNameString = re.sub(';', ',', chemspiderNameString)
                chemspiderNameString = re.sub('\n', ' ', chemspiderNameString)

                chemspiderMonoMass = chemspiderCompoundID.monoisotopicmass
                chemspiderMonoMassString = str(chemspiderMonoMass)

                chemspiderSumFormula = chemspiderCompoundID.mf
                chemspiderSumFormulaString = str(chemspiderSumFormula)
                Formula = re.sub(
                    r'[^a-zA-Z0-9\n\.]', '',
                    chemspiderSumFormulaString,
                )

                IDFvalue = 'n.a.'
                SpiderHit = 'n.a.'

                masserror = 1000 * (
                    parent_molecule_monopeakvalue_search - chemspiderMonoMass
                )
                if queryParameter['useIDFcheck']:
                    IDFvalue = IDF_checker(Formula, ionizationmode)
                    SpiderHit = math.pow(IDFvalue * masserror, 2)
                    IDFvalue = str('%f' % IDFvalue)
                    SpiderHit = str('%.12f' % SpiderHit)

                saveString = f"""{query_peak_mass} +/- {tolerance};{ionizationmode};ChemSpider DB (online);{chemspiderNameString};{CSID};{chemspiderNameString};{Formula};{chemspiderMonoMassString};{masserror};{IDFvalue};{SpiderHit}\n"""

                try:
                    SMresultfile.write(saveString)
                except:
                    print(f'\n{saveString}, caused an error!\n')

        except:
            print('No result from ChemSpider DB search!')


def search_spidermassDB(query_peak_mass, tolerance, ionizationmode, queryParameters):
    """[summary]

    Args:
        query_peak_mass ([type]): [description]
        tolerance ([type]): [description]
        ionizationmode ([type]): [description]
        queryParameters ([type]): [description]

    Returns:
        [type]: [description]
    """
    # load SpiderMassDB

    # Hardcoded for now - but should come from TK object
    spidermassdb = './dbase/streptomyces_meta_SpiderMassDB2.csv'
    with open(spidermassdb, encoding='utf-8') as file:
        entries = list(csv.DictReader(file, dialect='excel', delimiter=';'))
    entries = [dict(entry) for entry in entries]
    print(entries[2])

    print(
        f'Search for {query_peak_mass} \u00b1 {tolerance} {ionizationmode} in {spidermassdb}',
    )
    for entry in entries:
        if (
            (query_peak_mass - tolerance)
            <= float(entry['Monoisotopic_Mass'])
            <= (query_peak_mass + tolerance)
        ):
            print(f'match was {entry}')

            IDFvalue = 'n.a.'
            SpiderHit = 'n.a.'
            masserror = 1000 * (
                query_peak_mass
                - float(entry['Monoisotopic_Mass'])
            )

    return  # something!!!
    """if queryParameters['useIDFcheck']:
                IDFvalue = IDF_checker(Formula, ionizationmode)
                SpiderHit = math.pow(IDFvalue * masserror, 2)
                IDFvalue = str("%f" % IDFvalue)
                SpiderHit = str("%.12f" % SpiderHit)

    return

            SMresultwrite = f"{query_peak_mass} +/- {tolerance};
                {ionizationmode};{spidermassdbString};{ST};
                {CSID};{CN};{Formula};{MM};
                {masserror}; {IDFvalue}; {SpiderHit}\n"
            SMresultfile.write(SMresultwrite)"""


def select_spidermassdb():
    dbfilename = askopenfilename()
    SpiderMassDBfilename_value.set(dbfilename)


def select_MZpeaklist():
    plfilename = askopenfilename()
    MZpeaklistfilename_value.set(plfilename)


def de_novo_generator(
    ionizationmode, neutralmass, tolerance, elementOptions=defaultElementOptions,
):
    """[summary]

    Args:
        ionizationmode ([type]): [description]
        neutralmass ([type]): [description]
        tolerance ([type]): [description]
        elementOptions ([type], optional): [description]. Defaults to defaultElementOptions.

    Returns:
        [type]: [description]
    """
    neutralmass = int(neutralmass)
    print(f'De Novo Generator \n, {type(neutralmass)}')

    maxcountC = (neutralmass / 12) + 1
    if elementOptions['switchC']:
        print(f'Max. C: {maxcountC}')
        switchChr2 = f'-C 0-{maxcountC}'

    else:
        switchChr2 = '-C 0-0'

    if elementOptions['switch13C']:
        maxcount13C = (neutralmass / 13) + 1  # No 12C
        print(f'Max. 13C: {maxcount13C}')
        switch13Chr2 = str('-1 0-' + str(maxcount13C))

    else:
        switch13Chr2 = '-1 0-0'

    if elementOptions['switchH']:
        # assuming CH molecule with 4H/C
        maxcountH = (4 * neutralmass / 16) + 1
        print(f'Max. H: {maxcountH}')
        switchHhr2 = f'-H 0-{maxcountH}'

    else:
        switchHhr2 = '-H 0-0'

    if elementOptions['switchD']:
        # same ratio as for H: 4D/C but MW(D)=2
        maxcountD = (4 * neutralmass / 20) + 1
        print(f'Max. D: {maxcountD}')
        switchDhr2 = str('-D 0-' + str(maxcountD))

    else:
        switchDhr2 = '-D 0-0'

    if elementOptions['switchN']:
        maxcountN = int((2 * maxcountC / 3) + 1)  # for 2N/C
        print('Max. N: ', str(maxcountN))
        switchNhr2 = str('-N 0-' + str(maxcountN))

    else:
        switchNhr2 = '-N 0-0'

    if elementOptions['switch15N']:
        maxcount15N = int((2 * maxcountC / 3) + 1)  # same as N, but MW(15N)=15
        print('Max. N: ', str(maxcount15N))
        switch15Nhr2 = str('-M 0-' + str(maxcount15N))

    else:
        switch15Nhr2 = '-M 0-0'

    if elementOptions['switchO']:
        maxcountO = int((2 * maxcountC / 3) + 1)  # for 2O/C
        print('Max. O: ', str(maxcountO))
        switchOhr2 = str('-O 0-' + str(maxcountO))

    else:
        switchOhr2 = '-O 0-0'

    if elementOptions['switchF']:
        maxcountF = int((2 * maxcountC / 3) + 1)  # for 2F/C
        print('Max. F: ', str(maxcountF))
        switchFhr2 = str('-F 0-' + str(maxcountF))

    else:
        switchFhr2 = '-F 0-0'

    if elementOptions['switchNa']:
        maxcountNa = int((maxcountC / 2) + 1)  # for 1Na/C
        print('Max. Na: ', str(maxcountNa))
        switchNahr2 = str('-A 0-' + str(maxcountNa))

    else:
        switchNahr2 = '-A 0-0'

    if elementOptions['switchSi']:
        maxcountSi = int((maxcountC / 2) + 1)  # for 1Si/C
        print('Max. Si: ', str(maxcountSi))
        switchSihr2 = str('-A 0-' + str(maxcountSi))

    else:
        switchSihr2 = '-I 0-0'

    if elementOptions['switchP']:
        maxcountP = int((maxcountC / 2) + 1)  # for 1P/C
        print('Max. P: ', str(maxcountP))
        switchPhr2 = str('-P 0-' + str(maxcountP))

    else:
        switchPhr2 = '-P 0-0'

    if elementOptions['switchS']:
        maxcountS = int((maxcountC / 2) + 1)  # for 1S/C
        print('Max. S: ', str(maxcountS))
        switchShr2 = str('-S 0-' + str(maxcountS))

    else:
        switchShr2 = '-S 0-0'

    if elementOptions['switchCl']:
        maxcountCl = int((maxcountC / 2) + 1)  # for 1Cl/C
        print('Max. Cl: ', str(maxcountCl))
        switchClhr2 = str('-L 0-' + str(maxcountCl))

    else:
        switchClhr2 = '-L 0-0'

    if elementOptions['switchBr']:
        maxcountBr = int((maxcountC / 2) + 1)  # for 1Br/C
        print('Max. Br: ', str(maxcountBr))
        switchBrhr2 = str('-B 0-' + str(maxcountBr))

    else:
        switchBrhr2 = '-B 0-0'

    start = time.time()
    HR2mmass = str(neutralmass)
    HR2tolerance = f'{1000 * tolerance}'
    HR2options = f'-m {HR2mmass} -t {HR2tolerance}'
    print(HR2options)

    # Call external program, depending on operating system

    if sys.platform == 'linux' or sys.platform == 'linux2':
        call(
            [
                './smformula-linux',
                '-m ' + HR2mmass,
                '-t ' + HR2tolerance,
                switchChr2,
                switch13Chr2,
                switchHhr2,
                switchDhr2,
                switchNhr2,
                switch15Nhr2,
                switchOhr2,
                switchFhr2,
                switchNahr2,
                switchSihr2,
                switchPhr2,
                switchShr2,
                switchClhr2,
                switchBrhr2,
            ],
        )

    if sys.platform == 'win32':
        call(
            [
                'smformula-win.exe',
                '-m ' + HR2mmass,
                '-t ' + HR2tolerance,
                switchChr2,
                switch13Chr2,
                switchHhr2,
                switchDhr2,
                switchNhr2,
                switch15Nhr2,
                switchOhr2,
                switchFhr2,
                switchNahr2,
                switchSihr2,
                switchPhr2,
                switchShr2,
                switchClhr2,
                switchBrhr2,
            ],
        )

    elapsed = time.time() - start
    print(f'time elapsed for formula generation [s]: {elapsed}')
    print('importing results..')

    # dnDB = np.genfromtxt(
    #    "HR3.csv", delimiter=";", names=True, dtype=None, encoding="utf-8"
    # )

    with open('HR3.csv', newline='\n') as csvfile:
        entries = csv.DictReader(csvfile, delimiter=';')
        entries = [dict(entry) for entry in entries]

    return entries


"""
    if useIDFcheck:
        IDFvalue = int(IDF_checker(entry["Formula"], ionizationmode))
        entry["IDFvalue"] = f"{IDFvalue:.6f}"
        SpiderHit = math.pow(IDFvalue * int(float(entry["Mass_Error_mDa"])), 2)
        entry["SpiderHit"] = f"{SpiderHit:.12f}"
        saveString = (
            str(query_peak_mass)
            + "+/-"
            + str(tolerance)
            + ";"
            + ionizationmode
            + ";"
            + "HR3.csv"
            + ";"
            + "De Novo Formula"
            + ";"
            + "n.a."
            + ";"
            + "n.a."
            + ";"
            + entry["Formula"]
            + ";"
            + entry["Mass_Da"]
            + ";"
            + entry["Mass_Error_mDa"]
            + ";"
            + entry["IDFvalue"]
            + ";"
            + entry["SpiderHit"]
            + "\n"
        )
        SMresultfile.write(saveString)

 """


"""
class query_params:
    def __init__(self):
    '''All of the query specific settings
    '''
        self.MZpeaklist = MZpeaklistfilename_value.get()
        self.tolerance = float(tolerance_entry.get())
        self.hydrogencheck = hydrogencheck.get()
        self.useSMDBcheck = useSMDBcheck.get()
        self.useDNcheck = useDNcheck.get()
        self.sodiumcheck = sodiumcheck.get()
        self.minusHcheck = minusHcheck.get()
        self.useCScheck = useCScheck.get()
        self.useIDFcheck = useIDFcheck.get()
        self.potassiumcheck = potassiumcheck.get()
        self.ammoniumcheck = ammoniumcheck.get()
        self.zerocheck = zerocheck.get()
        self.minusHcheck = minusHcheck.get()
        self.query_peak_mass = float(monopeakvalue_entry.get())
        self.switchC = switchC.get()
        self.switch13C = switch13C.get()
        self.switchH = switchH.get()
        self.switchD = switchD.get()
        self.switchN = switchN.get()
        self.switch15N = switch15N.get()
        self.switch0 = switchO.get()
        self.switchF = switchF.get()
        self.switchNa = switchNa.get()
        self.switchSi = switchSi.get()
        self.switchP = switchP.get()
        self.switchS = switchS.get()
        self.switchCl = switchCl.get()
        self.switchBr = switchBr.get()
        self.CSmaxHits = CSmaxHits_set.get()
        self.EM0 = isoabundance1_set.get()
        self.EM1 = isoabundance2_set.get()
        self.EM2 = isoabundance3_set.get()
        self.EM3 = isoabundance4_set.get()
"""
