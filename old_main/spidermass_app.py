#    SpiderMass is a software for the automated generation of chemical databases and the identification of compounds.
#    Copyright (C) 2013-2017 Dr. Robert Winkler
#    email: robert.winkler@cinvestav.mx, robert.winkler@bioprocess.org
#    CINVESTAV Unidad Irapuato
#    Km. 9.6 Libramiento Norte Carr. Irapuato-León
#    36821 Irapuato Gto., México
#    Tel.: +52-462-6239-635
#    http://www.ira.cinvestav.mx/lababi.aspx
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
# Python specific imports
# Python wrapper for the ChemSpider API.
import re
import time
import tkinter.font as tkFont
import tkinter.messagebox as tkMessageBox
from subprocess import call
from tkinter import Button
from tkinter import Checkbutton
from tkinter import DoubleVar
from tkinter import E
from tkinter import Entry
from tkinter import Frame
from tkinter import IntVar
from tkinter import Label
from tkinter import Menu
from tkinter import StringVar
from tkinter import Tk
from tkinter import W
from tkinter.filedialog import askopenfilename

import numpy as np
from spidermassfunc import *

# from ctypes import *

# requests for ChemSpider API.
try:
    import chemspiderREST
except:
    print('You are OFFLINE. ChemSpider REST requests will not work.')


def Quit_SpiderMass():
    quit()


def main():

    fontExample = tkFont.Font(family='Mono', size=12, weight='bold')

    # creating a menu

    # Action Buttons

    action_button = Button(features, text='Quit', command=Quit_SpiderMass)
    action_button.grid(row=80, column=9)

    root.mainloop()


if __name__ == '__main__':
    main()


# Definition of functions


def help_usage():
    tkMessageBox.showinfo(
        'Help', "Read the README file \nJust try, it's easy!",
    )


# Definition of global variables

# Main program

# Main Window

root = Tk()
root.title('SpiderMass Identifier 1.95')

# GUI

features = Frame(root)
features.grid(sticky=W)

# INPUT area
instruction_label = Label(
    features,
    text='Input a mass and tolerance in the fields below or upload a file containing the imformation...',
)
instruction_label.grid(row=0, column=1, sticky=W)

header_label = Label(features, text='INPUT (Single Peak, or list):')
header_label.grid(row=3, column=1, sticky=W)

# m/z read from the spectrum
monopeak_label = Label(features, text='Mono peak M [m/z]')
monopeak_label.grid(row=1, column=2, sticky=W)

monopeak_set = DoubleVar()
monopeak_set.set(323.0196)

monopeakvalue_entry = Entry(features, textvariable=monopeak_set)
monopeakvalue_entry.grid(row=3, column=2, sticky=W)

monopeaktolerance_label = Label(
    features, text='Mass error [m/z]',
)  # mass accuracy
monopeaktolerance_label.grid(row=1, column=3, sticky=W)

monotolerance_set = DoubleVar()
monotolerance_set.set(0.001)

monopeaktolerance_entry = Entry(features, textvariable=monotolerance_set)
monopeaktolerance_entry.grid(row=3, column=3, sticky=W)

denovo_label = Label(features, text='For IDF ->')  # Isotope
denovo_label.grid(row=3, column=4, sticky=W)

isotopeabundance_label = Label(
    features, text='Isotope abundance:',
)  # Isotope abunance
isotopeabundance_label.grid(row=3, column=5, sticky=W)

isotopeabundance_label1 = Label(features, text='M [% or absolute]')
isotopeabundance_label1.grid(row=1, column=6, sticky=W)

isotopeabundance_label2 = Label(features, text='M+1 [% or absolute]')
isotopeabundance_label2.grid(row=1, column=7, sticky=W)

isotopeabundance_label3 = Label(features, text='M+2 [% or absolute]')
isotopeabundance_label3.grid(row=1, column=8, sticky=W)

isotopeabundance_label4 = Label(features, text='M+3 [% or absolute]')
isotopeabundance_label4.grid(row=1, column=9, sticky=W)

isoabundance1_set = DoubleVar()
isoabundance1_set.set(100)

isotopeabundance_entry1 = Entry(features, textvariable=isoabundance1_set)
isotopeabundance_entry1.grid(row=3, column=6, sticky=W)

isoabundance2_set = DoubleVar()
isoabundance2_set.set(13)

isotopeabundance_entry2 = Entry(features, textvariable=isoabundance2_set)
isotopeabundance_entry2.grid(row=3, column=7, sticky=W)

isoabundance3_set = DoubleVar()
isoabundance3_set.set(66)

isotopeabundance_entry3 = Entry(features, textvariable=isoabundance3_set)
isotopeabundance_entry3.grid(row=3, column=8, sticky=W)

isoabundance4_set = DoubleVar()
isoabundance4_set.set(8)

isotopeabundance_entry4 = Entry(features, textvariable=isoabundance4_set)
isotopeabundance_entry4.grid(row=3, column=9, sticky=W)

# Peak list selection

# Ion checkboxes

ions_label = Label(features, text='   Ion(s):')
ions_label.grid(row=5, column=1, sticky=E)

hydrogencheck = IntVar()
hydrogencheckbox = Checkbutton(
    features, text='[M+H]+', variable=hydrogencheck, onvalue=1, offvalue=0, height=2,
)
hydrogencheckbox.grid(row=5, column=2, sticky=W)
hydrogencheckbox.select()

sodiumcheck = IntVar()
sodiumcheckbox = Checkbutton(
    features, text='[M+Na]+', variable=sodiumcheck, onvalue=1, offvalue=0, height=2,
)
sodiumcheckbox.grid(row=5, column=3, sticky=W)
sodiumcheckbox.select()

potassiumcheck = IntVar()
potassiumcheckbox = Checkbutton(
    features, text='[M+K]+', variable=potassiumcheck, onvalue=1, offvalue=0, height=2,
)
potassiumcheckbox.grid(row=5, column=4, sticky=W)
# potassiumcheckbox.select()

ammoniumcheck = IntVar()
ammoniumcheckbox = Checkbutton(
    features, text='[M+NH4]+', variable=ammoniumcheck, onvalue=1, offvalue=0, height=2,
)
ammoniumcheckbox.grid(row=5, column=5, sticky=W)
# ammoniumcheckbox.select()

zerocheck = IntVar()
zerocheckbox = Checkbutton(
    features, text='+/- 0', variable=zerocheck, onvalue=1, offvalue=0, height=2,
)
zerocheckbox.grid(row=5, column=6, sticky=W)
# zerocheckbox.select()

minusHcheck = IntVar()
minusHcheckbox = Checkbutton(
    features, text='[M-H]-', variable=minusHcheck, onvalue=1, offvalue=0, height=2,
)
minusHcheckbox.grid(row=5, column=7, sticky=W)
# minusHcheckbox.select()

# SPIDERMASS MODULE

useSMDBcheck = IntVar()
useSMDBcheckbox = Checkbutton(
    features,
    text='SPIDERMASS DATABASE SEARCH',
    variable=useSMDBcheck,
    onvalue=1,
    offvalue=0,
    height=2,
)
useSMDBcheckbox.grid(row=6, column=1, sticky=W)
# useSMDBcheckbox.select()

# SpidermassDB selection


SpiderMassDBfilename_value = StringVar()
SpiderMassDBfilename_value.set('./dbase/streptomyces_meta_SpiderMassDB.csv')
SpiderMassDBfilename_output = Entry(
    features, textvariable=SpiderMassDBfilename_value, width=160,
)
SpiderMassDBfilename_output.grid(row=7, column=2, columnspan=8)


# CHEMSPIDER MODULE

useCScheck = IntVar()
useCScheckbox = Checkbutton(
    features,
    text='CHEMSPIDER.COM SEARCH',
    variable=useCScheck,
    onvalue=1,
    offvalue=0,
    height=2,
)
useCScheckbox.grid(row=9, column=1, sticky=W)
# useCScheckbox.select()

# set timeout for ChemSpider.com Online Search
CStimeout_label = Label(features, text='timeout [s]:')
CStimeout_label.grid(row=9, column=2, sticky=W)

CStimeout_set = IntVar()
CStimeout_set.set(3)

CStimeoutvalue_entry = Entry(features, textvariable=CStimeout_set)
CStimeoutvalue_entry.grid(row=9, column=3, sticky=W)

# Set max. hits reported from ChemSpider.com online search

CSmaxHits_label = Label(features, text='max. hits:')
CSmaxHits_label.grid(row=9, column=4, sticky=W)

CSmaxHits_set = IntVar()
CSmaxHits_set.set(10)

CSmaxHitsvalue_entry = Entry(features, textvariable=CSmaxHits_set)
CSmaxHitsvalue_entry.grid(row=9, column=5, sticky=W)
# De-Novo MODULE

useDNcheck = IntVar()
useDNcheckbox = Checkbutton(
    features,
    text='DE-NOVO FORMULA BUILDER',
    variable=useDNcheck,
    onvalue=1,
    offvalue=0,
    height=2,
)
useDNcheckbox.grid(row=10, column=1, sticky=W)
useDNcheckbox.select()

DNelements_label = Label(features, text='   Elements for De-Novo:')
DNelements_label.grid(row=11, column=1, sticky=E)

switchC = IntVar()
switchCcheckbox = Checkbutton(
    features, text='C', variable=switchC, onvalue=1, offvalue=0, height=2,
)
switchCcheckbox.grid(row=11, column=2, sticky=W)
switchCcheckbox.select()

switch13C = IntVar()
switch13Ccheckbox = Checkbutton(
    features, text='13C', variable=switch13C, onvalue=1, offvalue=0, height=2,
)
switch13Ccheckbox.grid(row=11, column=3, sticky=W)
# switch13Ccheckbox.select()

switchH = IntVar()
switchHcheckbox = Checkbutton(
    features, text='H', variable=switchH, onvalue=1, offvalue=0, height=2,
)
switchHcheckbox.grid(row=11, column=4, sticky=W)
switchHcheckbox.select()

switchD = IntVar()
switchDcheckbox = Checkbutton(
    features, text='D', variable=switchD, onvalue=1, offvalue=0, height=2,
)
switchDcheckbox.grid(row=11, column=5, sticky=W)
# switchDcheckbox.select()

switchN = IntVar()
switchNcheckbox = Checkbutton(
    features, text='N', variable=switchN, onvalue=1, offvalue=0, height=2,
)
switchNcheckbox.grid(row=11, column=6, sticky=W)
switchNcheckbox.select()

switch15N = IntVar()
switch15Ncheckbox = Checkbutton(
    features, text='15N', variable=switch15N, onvalue=1, offvalue=0, height=2,
)
switch15Ncheckbox.grid(row=11, column=7, sticky=W)
# switch15Ncheckbox.select()

switchO = IntVar()
switchOcheckbox = Checkbutton(
    features, text='O', variable=switchO, onvalue=1, offvalue=0, height=2,
)
switchOcheckbox.grid(row=11, column=8, sticky=W)
switchOcheckbox.select()

switchF = IntVar()
switchFcheckbox = Checkbutton(
    features, text='F', variable=switchF, onvalue=1, offvalue=0, height=2,
)
switchFcheckbox.grid(row=12, column=2, sticky=W)
# switchFcheckbox.select()

switchNa = IntVar()
switchNacheckbox = Checkbutton(
    features, text='Na', variable=switchNa, onvalue=1, offvalue=0, height=2,
)
switchNacheckbox.grid(row=12, column=3, sticky=W)
# switchNacheckbox.select()

switchSi = IntVar()
switchSicheckbox = Checkbutton(
    features, text='Si', variable=switchSi, onvalue=1, offvalue=0, height=2,
)
switchSicheckbox.grid(row=12, column=4, sticky=W)
# switchSicheckbox.select()

switchP = IntVar()
switchPcheckbox = Checkbutton(
    features, text='P', variable=switchP, onvalue=1, offvalue=0, height=2,
)
switchPcheckbox.grid(row=12, column=5, sticky=W)
# switchPcheckbox.select()

switchS = IntVar()
switchScheckbox = Checkbutton(
    features, text='S', variable=switchS, onvalue=1, offvalue=0, height=2,
)
switchScheckbox.grid(row=12, column=6, sticky=W)
# switchScheckbox.select()

switchCl = IntVar()
switchClcheckbox = Checkbutton(
    features, text='Cl', variable=switchCl, onvalue=1, offvalue=0, height=2,
)
switchClcheckbox.grid(row=12, column=7, sticky=W)
switchClcheckbox.select()

switchBr = IntVar()
switchBrcheckbox = Checkbutton(
    features, text='Br', variable=switchBr, onvalue=1, offvalue=0, height=2,
)
switchBrcheckbox.grid(row=12, column=8, sticky=W)
# switchBrcheckbox.select()

# ISOTOPE DISTRIBUTION FIT
useIDFcheck = IntVar()
useIDFcheckbox = Checkbutton(
    features,
    text='ISOTOPE DISTRIBUTION FIT (IDF)',
    variable=useIDFcheck,
    onvalue=1,
    offvalue=0,
    height=2,
)
useIDFcheckbox.grid(row=13, column=1, sticky=W)
useIDFcheckbox.select()

Empty_label = Label(features, text='   ')
Empty_label.grid(row=79, column=1, sticky=E)

# Action Buttons
action_button = Button(
    features,
    text='Start SpiderMass ID',
    command=ion_search(),  # 300.001, 0.001, ["N+H"]),
    # monopeakvalue_entry.get(),
    # monopeaktolerance_entry.get(),
    # ionSpecies,
    # parameter_dict=queryParameters,
    # elementOptions=defaultElementOptions,
)
action_button.grid(row=80, column=1)

action_button = Button(features, text='Quit', command=Quit_SpiderMass)
action_button.grid(row=80, column=9)

# creating a menu

menu = Menu(root)
root.config(menu=menu)

filemenu = Menu(menu)
menu.add_cascade(label='File', menu=filemenu)
filemenu.add_command(label='Exit', command=Quit_SpiderMass)

spidermenu = Menu(menu)
menu.add_cascade(label='SpiderMass Database', menu=spidermenu)
# spidermenu.add_command(label="DB Generator", command=start_dbgenerator)

optionmenu = Menu(menu)
menu.add_cascade(label='Options', menu=optionmenu)
# optionmenu.add_command(label="Check Operating System", command=check_OS)

helpmenu = Menu(menu)
menu.add_cascade(label='Help', menu=helpmenu)
# helpmenu.add_command(label="Usage", command=help_usage)
# helpmenu.add_command(label="About...", command=help_about)

root.mainloop()
