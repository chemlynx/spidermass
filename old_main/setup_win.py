import os
import sys

import numpy
import osa
from cx_Freeze import Executable
from cx_Freeze import setup

os.environ['TCL_LIBRARY'] = 'C:\\Python36-32\\tcl\\tcl8.6'
os.environ['TK_LIBRARY'] = 'C:\\Python36-32\\tcl\\tk8.6'

# adjust the file location, python folder name and username

files = {
    'include_files': [
        'C:\\Python36-32\\vcruntime140.dll',
        'C:\\Python36-32\\DLLs\\tcl86t.dll',
        'C:\\Python36-32\\DLLs\\tk86t.dll',
        'C:\\Python36-32\\tcl\\tk8.6',
        'C:\\Python36-32\\tcl\\tcl8.6',
    ],
    'packages': ['tkinter', 'osa', 'numpy'],
}

# if sys.platform == 'win32':
#    base = "Win32GUI"
base = None

executables = [Executable('spidermass.py', base=base)]

setup(
    name='SpiderMass',
    version='1.1',
    description='Semantic MS database generator and de-novo formula generation',
    options={'build_exe': files},
    executables=executables,
)
