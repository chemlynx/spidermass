#    SpiderMass is a software for the automated generation of chemical databases and the identification of compounds.
#    Copyright (C) 2013-2017 Dr. Robert Winkler
#    email: robert.winkler@cinvestav.mx, robert.winkler@bioprocess.org
#    CINVESTAV Unidad Irapuato
#    Km. 9.6 Libramiento Norte Carr. Irapuato-León
#    36821 Irapuato Gto., México
#    Tel.: +52-462-6239-635
#    http://www.ira.cinvestav.mx/lababi.aspx
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
# The code was initially written by Matt Swain (m.swain@me.com) and Cameron Neylon
# https://github.com/mcs07/ChemSpiPy
# https://github.com/cameronneylon/ChemSpiPy
# ChemSpiPy is a Python wrapper for the ChemSpider API and was forked by Robert Winkler
# for integration into SpiderMass.
# Parts of the code were originally licensed under the MIT License:
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# FOR USING THIS LIBRARY YOU HAVE TO REGISTER AT CHEMSPIDER
# (FREE FOR ACADEMICS) AND PUT THE TOLKEN IN A FILE CALLED
# chemspider-token.txt in the main directory of SpiderMass
import json
import sys
import time

import requests
from chemspiderREST import headers as headers


class Compound:
    """A class for retrieving record details about a compound by CSID.

    The purpose of this class is to provide access to various parts of the
    ChemSpider API that return information about a compound given its CSID.
    Information is loaded lazily when requested, and cached for future access.
    """

    def __init__(self, csid):
        """Initialize with a CSID as an int or str or using json."""
        if type(csid) is str and csid.isdigit():
            self.csid = csid
        elif type(csid) is int:
            self.csid = str(csid)
        else:
            raise TypeError(
                'Compound must be initialised with a CSID as an int or str',
            )

        self._imageurl = None
        self._mf = None
        self._smiles = None
        self._inchi = None
        self._inchikey = None
        self._averagemass = None
        self._molecularweight = None
        self._monoisotopicmass = None
        self._nominalmass = None
        self._stdinchi = None
        self._stdinchikey = None
        self._commonname = None
        self._image = None
        self._mol = None
        self._mol3D = None

    """ @classmethod
    def from_csid():
        pass


    @classmethod
    def from_json(self, parameter_list):
    pass """

    def __repr__(self):
        """The repr for the compound object.

        Returns:
            [type]: [description]
        """
        return f'Compound({self.csid})'

    @property
    def imageurl(self):
        """Return the URL of a png image of the 2D structure."""
        if self._imageurl is None:
            self._imageurl = (
                f'http://www.chemspider.com/ImagesHandler.ashx?id={self.csid}s'
            )
        return self._imageurl

    @property
    def mf(self):
        """Retrieve molecular formula from ChemSpider"""
        if self._mf is None:
            self.loadextendedcompoundinfo()
        return self._mf

    @property
    def smiles(self):
        """Retrieve SMILES string from ChemSpider"""
        if self._smiles is None:
            self.loadextendedcompoundinfo()
        return self._smiles

    @property
    def inchi(self):
        """Retrieve InChi string from ChemSpider"""
        if self._inchi is None:
            self.loadextendedcompoundinfo()
        return self._inchi

    @property
    def inchikey(self):
        """Retrieve InChi string from ChemSpider"""
        if self._inchikey is None:
            self.loadextendedcompoundinfo()
        return self._inchikey

    @property
    def stdinchi(self):
        """Retrieve stdInChi string from ChemSpider"""
        if self._stdinchi is None:
            self.loadextendedcompoundinfo()
        return self._stdinchi

    @property
    def stdinchikey(self):
        """Retrieve stdInChi string from ChemSpider"""
        if self._stdinchikey is None:
            self.loadextendedcompoundinfo()
        return self._stdinchikey

    @property
    def averagemass(self):
        """Retrieve average mass from ChemSpider"""
        if self._averagemass is None:
            self.loadextendedcompoundinfo()
        return self._averagemass

    @property
    def molecularweight(self):
        """Retrieve molecular weight from ChemSpider"""
        if self._molecularweight is None:
            self.loadextendedcompoundinfo()
        return self._molecularweight

    @property
    def monoisotopicmass(self):
        """Retrieve monoisotropic mass from ChemSpider"""
        if self._monoisotopicmass is None:
            self.loadextendedcompoundinfo()
        return self._monoisotopicmass

    @property
    def nominalmass(self):
        """Retrieve nominal mass from ChemSpider"""
        if self._nominalmass is None:
            self.loadextendedcompoundinfo()
        return self._nominalmass

    @property
    def mol(self):
        """Return record in MOL format"""
        if self._mol is None:
            self.loadextendedcompoundinfo()
        return self._mol

    @property
    def mol3D(self):
        """Return record in MOL format with 3D coordinates calculated"""
        if self._mol3D is None:
            self.loadextendedcompoundinfo()
        return self._mol3D

    @property
    def commonname(self):
        """Retrieve common name from ChemSpider"""
        if self._commonname is None:
            self.loadextendedcompoundinfo()
        return self._commonname

    def loadextendedcompoundinfo(self):
        """Load extended compound info from the Mass Spec API"""

        global headers

        fields = 'SMILES%2CFormula%2CInChI%2CInChIKey%2CStdInChI%2CStdInChIKey%2CAverageMass%2CMolecularWeight%2CMonoisotopicMass%2CNominalMass%2CCommonName%2CReferenceCount%2CDataSourceCount%2CPubMedCount%2CRSCCount%2CMol2D%2CMol3D'
        recordDetailsUrl = f'https://api.rsc.org/compounds/v1/records/{self.csid}/details?fields={fields}'
        response = requests.get(recordDetailsUrl, headers=headers)
        if response.status_code != 200:
            print(
                f'there was an error with your attempt to get record data, the request failed with the following error:\n{response.status_code}: {response.reason}',
            )
        else:
            recordDetails = json.loads(response.text)
            mf = recordDetails['formula']
            self._mf = mf if mf is not None else None
            smiles = recordDetails['smiles']
            self._smiles = smiles if smiles is not None else None
            inchi = recordDetails['inchi']
            self._inchi = inchi if inchi is not None else None
            inchikey = recordDetails['inchiKey']
            self._inchikey = inchikey if inchikey is not None else None
            stdinchi = recordDetails['stdinchi']
            self._stdinchi = stdinchi if stdinchi is not None else None
            stdinchikey = recordDetails['stdinchiKey']
            self._stdinchikey = stdinchikey if stdinchikey is not None else None
            averagemass = recordDetails['averageMass']
            self._averagemass = float(
                averagemass,
            ) if averagemass is not None else None
            molecularweight = recordDetails['molecularWeight']
            self._molecularweight = (
                float(molecularweight) if molecularweight is not None else None
            )
            monoisotopicmass = recordDetails['monoisotopicMass']
            self._monoisotopicmass = (
                float(monoisotopicmass) if monoisotopicmass is not None else None
            )
            nominalmass = recordDetails['nominalMass']
            self._nominalmass = float(
                nominalmass,
            ) if nominalmass is not None else None
            commonname = recordDetails['commonName']
            self._commonname = commonname if commonname is not None else None
            mol = recordDetails['mol2D']
            self._mol = mol if mol is not None else None
            mol3D = recordDetails['mol3D']
            self._mol3D = mol3D if mol3D is not None else None

    '''    def loadextendedcompoundinfoJSON(self, RSCJson):
        """ Load extended compound info from json dict """
        recordDetails
        mf = recordDetails["formula"]
        self._mf = mf if mf is not None else None
        smiles = recordDetails["smiles"]
        self._smiles = smiles if smiles is not None else None
        inchi = recordDetails["inchi"]
        self._inchi = inchi if inchi is not None else None
        inchikey = recordDetails["inchiKey"]
        self._inchikey = inchikey if inchikey is not None else None
        stdinchi = recordDetails["stdinchi"]
        self._stdinchi = stdinchi if stdinchi is not None else None
        stdinchikey = recordDetails["stdinchiKey"]
        self._stdinchikey = stdinchikey if stdinchikey is not None else None
        averagemass = recordDetails["averageMass"]
        self._averagemass = float(averagemass) if averagemass is not None else None
        molecularweight = recordDetails["molecularWeight"]
        self._molecularweight = (
            float(molecularweight) if molecularweight is not None else None
        )
        monoisotopicmass = recordDetails["monoisotopicMass"]
        self._monoisotopicmass = (
            float(monoisotopicmass) if monoisotopicmass is not None else None
        )
        nominalmass = recordDetails["nominalMass"]
        self._nominalmass = float(nominalmass) if nominalmass is not None else None
        commonname = recordDetails["commonName"]
        self._commonname = commonname if commonname is not None else None
        mol = recordDetails["mol2D"]
        self._mol2D = mol if mol is not None else None
        mol3d = recordDetails["mol3D"]
        self._mol3D = mol3d if mol3d is not None else None '''

    @property
    def image(self):
        """Return string containing PNG binary image data of 2D structure image"""
        if self._image is None:
            global headers
            recordDetailsUrl = (
                f'https://api.rsc.org/compounds/v1/records/{self.csid}/image'
            )
            response = requests.get(recordDetailsUrl, header=headers)
            if response.status_code != 200:
                print(
                    f'there was an error with your attempt to get record data, the request failed with the following error:\n{response.status_code}: {response.reason}',
                )
                self._image = json.loads(response.text)
        return self._image


def find(query):
    """Search by Name, SMILES, InChI, InChIKey, etc. Returns first 100 Compounds"""

    global headers
    recordDetailsUrl = 'https://api.rsc.org/compounds/v1/filter/name'
    body = {'name': query}
    response = requests.post(recordDetailsUrl, json=body, headers=headers)
    if response.status_code != 200:
        print(
            f'there was an error with your attempt to search for: {query}, the request failed with the following error:\n{response.status_code}: {response.reason}',
        )
    query_id_json = json.loads(response.text)
    query_id = query_id_json['queryId']

    status_url = f'https://api.rsc.org/compounds/v1/filter/{query_id}/status'

    status_response = requests.get(status_url, headers=headers)
    searchStatus = json.loads(status_response.text)
    searchStatus = searchStatus['status']
    print(f'Status of ChemSpider REST request: {searchStatus}')

    if searchStatus == 'Processing':
        time.sleep(1)

    elif searchStatus == 'Complete':
        results_url = f'https://api.rsc.org/compounds/v1/filter/{query_id}/results'
        print(results_url)

        results_response = requests.get(results_url, headers=headers)
        searchResults = json.loads(results_response.text)

    if len(searchResults['results']) == 0:
        compounds = None
    compounds = [Compound(csid) for csid in searchResults['results']]

    return compounds


def find_one(query):
    """Search by Name, SMILES, InChI, InChIKey, etc. Returns a single Compound"""
    compoundlist = find(query)
    return compoundlist[0] if compoundlist else None


def formula_find(query, SearchTimeOut):
    """Search by Sum Formula"""
    global headers
    assert type(query) == str or type(
        query,
    ) == str, 'query not a string object'

    print(
        f"""Searching for formula {query} in ChemSpider""",
    )

    formulaSearchParameters = {
        'orderBy': 'dataSourceCount',
        'orderDirection': 'descending',
        'formula': query,
    }
    # print(monoisotopicMassParameters)

    formulaSearchURL = 'https://api.rsc.org/compounds/v1/filter/formula'

    response = requests.post(
        formulaSearchURL, json=formulaSearchParameters, headers=headers,
    )
    print(response.text)
    time.sleep(1)
    SearchTimeOut = int(SearchTimeOut + 1)

    for i in range(1, SearchTimeOut):

        if response.status_code == 200:
            query_id_json = json.loads(response.text)
            query_id = query_id_json['queryId']
            print(query_id)
            status_url = f'https://api.rsc.org/compounds/v1/filter/{query_id}/status'

            status_response = requests.get(status_url, headers=headers)
            searchStatus = json.loads(status_response.text)
            searchStatus = searchStatus['status']

            print(f'Status of ChemSpider REST request: {searchStatus}')
        else:
            print(response.reason, response.status_code)

        if searchStatus == 'Processing':
            time.sleep(1)

        if searchStatus == 'Complete':
            # rsc-test-uat.apigee.net
            results_url = f'https://api.rsc.org/compounds/v1/filter/{query_id}/results'
            print(results_url)

            results_response = requests.get(results_url, headers=headers)
            searchResults = json.loads(results_response.text)
            resultCSint = searchResults
            print('Obtaining results..')
            break

        if i == (SearchTimeOut - 1):
            resultCSint = ['empty']
            print(f'Timeout: >{SearchTimeOut - 1}s without result')
            break

        if searchStatus not in ('Complete', 'Processing'):
            resultCSint = None
            break

    compoundlist = searchResults['results']
    return compoundlist if len(compoundlist) > 0 else None
