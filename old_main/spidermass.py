#    SpiderMass is a software for the automated generation of chemical databases and the identification of compounds.
#    Copyright (C) 2013-2017 Dr. Robert Winkler
#    email: robert.winkler@cinvestav.mx, robert.winkler@bioprocess.org
#    CINVESTAV Unidad Irapuato
#    Km. 9.6 Libramiento Norte Carr. Irapuato-León
#    36821 Irapuato Gto., México
#    Tel.: +52-462-6239-635
#    http://www.ira.cinvestav.mx/lababi.aspx
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
# Python specific imports
import csv
import math
import ntpath
import re
import sys
import time
import tkinter.messagebox as tkMessageBox
from subprocess import call
from tkinter import Button
from tkinter import Checkbutton
from tkinter import DoubleVar
from tkinter import E
from tkinter import Entry
from tkinter import Frame
from tkinter import IntVar
from tkinter import Label
from tkinter import Menu
from tkinter import StringVar
from tkinter import Tk
from tkinter import W
from tkinter.filedialog import askopenfilename

import numpy as np

# Python wrapper for the ChemSpider API.
# from ctypes import *

# requests for ChemSpider API.
try:
    import chemspiderREST
except:
    print('You are OFFLINE. ChemSpider REST requests will not work.')


# Definition of functions


def help_usage():
    tkMessageBox.showinfo(
        'Help', "Read the README file \nJust try, it's easy!",
    )


def help_about():
    tkMessageBox.showinfo(
        'About',
        """SpiderMass Identifier 1.1 \nLicense: GPLv3 \nRobert Winkler\n
        CINVESTAV Irapuato, Mexico, 2013\nhttp://www.ira.cinvestav.mx/lababi.aspx""",
    )


def check_OS():
    string_detected_OS = 'SpiderMass is running on ' + sys.platform
    print(string_detected_OS)
    tkMessageBox.showinfo('Operating System', string_detected_OS)


def start_dbgenerator():
    import spidermass_dbgenerator

    spidermass_dbgenerator()


def ion_search():
    # Get ChemSpider.com online search timeout
    CStimeout = CStimeout_set.get()

    # monoisotopic molecular weight (MMW) of elements/molecules
    isotopesdb = './isotopesdb/NIST.csv'
    isoDB = np.genfromtxt(
        isotopesdb, delimiter=';', names=True, dtype=None, encoding='utf-8',
    )
    numrows_isoDB = len(isoDB)
    E_isoDB = isoDB['element_E']
    RAM_isoDB = isoDB['relative_atomic_mass']

    MMW_H = 0
    MMW_Na = 0
    MMW_K = 0
    MMW_N = 0

    for j in range(0, numrows_isoDB):
        if E_isoDB[j] == 'e':
            MMW_e = RAM_isoDB[j]
        if E_isoDB[j] == 'H':
            if MMW_H == 0:
                MMW_H = RAM_isoDB[j]
        if E_isoDB[j] == 'Na':
            if MMW_Na == 0:
                MMW_Na = RAM_isoDB[j]
        if E_isoDB[j] == 'K':
            if MMW_K == 0:
                MMW_K = RAM_isoDB[j]
        if E_isoDB[j] == 'N':
            if MMW_N == 0:
                MMW_N = RAM_isoDB[j]
    MMW_NH4 = MMW_N + (4 * MMW_H)
    print(
        f"""Electron weight: {MMW_e}
        H weight: {MMW_H}
        Na weight: {MMW_Na}
        K weight: {MMW_K}
        N weight: {MMW_N}
        NH4 weight: {MMW_NH4}""",
    )

    MZpeaklist = MZpeaklistfilename_value.get()

    # Evaluation of mass list

    if MZpeaklist:

        # No IDF supported for masslists
        useIDFcheckbox.deselect()

        SMresultfile = open('SpiderMassResults.csv', 'w')
        SMresultwrite = str(
            'm/z'
            + ' +/- '
            + 'Mass Tolerance'
            + ';'
            + 'Ionization Mode'
            + ';'
            + 'Database'
            + ';'
            + 'Name'
            + ';'
            + 'ChemSpider ID'
            + ';'
            + 'Common Name (ChemSpider)'
            + ';'
            + 'Formula'
            + ';'
            + 'Monoisotopic Mass [Da]'
            + ';'
            + 'Mass Error [mDa]'
            + ';'
            + 'Isotope Distribution Fit (IDF)'
            + ';'
            + 'Spider Hit'
            + '\n',
        )
        SMresultfile.write(SMresultwrite)

        with open(MZpeaklist) as pf:
            for line in pf.readlines():
                print(line)
                monopeakvalue_search = float(line)
                monopeaktolerance = float(monopeaktolerance_entry.get())

                if hydrogencheck.get() == 1:
                    ionizationmode = '[M+H]+'
                    parent_molecule_monopeakvalue_search = (
                        monopeakvalue_search - MMW_H + MMW_e
                    )
                    lowermasslimitMZ = (
                        monopeakvalue_search - monopeaktolerance - MMW_H + MMW_e
                    )
                    uppermasslimitMZ = (
                        monopeakvalue_search + monopeaktolerance - MMW_H + MMW_e
                    )

                    if useSMDBcheck.get() == 1:
                        search_spidermassDB(
                            SMresultfile,
                            monopeakvalue_search,
                            monopeaktolerance,
                            ionizationmode,
                            lowermasslimitMZ,
                            uppermasslimitMZ,
                        )
                    if useCScheck.get() == 1:
                        RESTrequest = chemspiderREST.IntrinsicPropSeachMonoMWREST(
                            parent_molecule_monopeakvalue_search,
                            monopeaktolerance,
                            CStimeout,
                        )
                        chemspider_decoder(
                            RESTrequest,
                            SMresultfile,
                            monopeakvalue_search,
                            monopeaktolerance,
                            ionizationmode,
                            parent_molecule_monopeakvalue_search,
                        )
                    if useDNcheck.get() == 1:
                        neutralmass = monopeakvalue_search - MMW_H + MMW_e
                        de_novo_generator(
                            SMresultfile,
                            monopeakvalue_search,
                            ionizationmode,
                            neutralmass,
                            monopeaktolerance,
                        )

                if sodiumcheck.get() == 1:
                    ionizationmode = '[M+Na]+'
                    parent_molecule_monopeakvalue_search = (
                        monopeakvalue_search - MMW_Na + MMW_e
                    )
                    lowermasslimitMZ = (
                        monopeakvalue_search - monopeaktolerance - MMW_Na + MMW_e
                    )
                    uppermasslimitMZ = (
                        monopeakvalue_search + monopeaktolerance - MMW_Na + MMW_e
                    )
                    if useSMDBcheck.get() == 1:
                        search_spidermassDB(
                            SMresultfile,
                            monopeakvalue_search,
                            monopeaktolerance,
                            ionizationmode,
                            lowermasslimitMZ,
                            uppermasslimitMZ,
                        )
                    if useCScheck.get() == 1:
                        RESTrequest = chemspiderREST.IntrinsicPropSeachMonoMWREST(
                            parent_molecule_monopeakvalue_search,
                            monopeaktolerance,
                            CStimeout,
                        )
                        chemspider_decoder(
                            RESTrequest,
                            SMresultfile,
                            monopeakvalue_search,
                            monopeaktolerance,
                            ionizationmode,
                            parent_molecule_monopeakvalue_search,
                        )
                    if useDNcheck.get() == 1:
                        neutralmass = monopeakvalue_search - MMW_Na + MMW_e
                        de_novo_generator(
                            SMresultfile,
                            monopeakvalue_search,
                            ionizationmode,
                            neutralmass,
                            monopeaktolerance,
                        )

                if potassiumcheck.get() == 1:
                    ionizationmode = '[M+K]+'
                    parent_molecule_monopeakvalue_search = (
                        monopeakvalue_search - MMW_K + MMW_e
                    )
                    lowermasslimitMZ = (
                        monopeakvalue_search - monopeaktolerance - MMW_K + MMW_e
                    )
                    uppermasslimitMZ = (
                        monopeakvalue_search + monopeaktolerance - MMW_K + MMW_e
                    )
                    if useSMDBcheck.get() == 1:
                        search_spidermassDB(
                            SMresultfile,
                            monopeakvalue_search,
                            monopeaktolerance,
                            ionizationmode,
                            lowermasslimitMZ,
                            uppermasslimitMZ,
                        )
                    if useCScheck.get() == 1:
                        RESTrequest = chemspiderREST.IntrinsicPropSeachMonoMWREST(
                            parent_molecule_monopeakvalue_search,
                            monopeaktolerance,
                            CStimeout,
                        )
                        chemspider_decoder(
                            RESTrequest,
                            SMresultfile,
                            monopeakvalue_search,
                            monopeaktolerance,
                            ionizationmode,
                            parent_molecule_monopeakvalue_search,
                        )
                    if useDNcheck.get() == 1:
                        neutralmass = monopeakvalue_search - MMW_K + MMW_e
                        de_novo_generator(
                            SMresultfile,
                            monopeakvalue_search,
                            ionizationmode,
                            neutralmass,
                            monopeaktolerance,
                        )

                if ammoniumcheck.get() == 1:
                    ionizationmode = '[M+NH4]+'
                    parent_molecule_monopeakvalue_search = (
                        monopeakvalue_search - MMW_NH4 + MMW_e
                    )
                    lowermasslimitMZ = (
                        monopeakvalue_search - monopeaktolerance - MMW_NH4 + MMW_e
                    )
                    uppermasslimitMZ = (
                        monopeakvalue_search + monopeaktolerance - MMW_NH4 + MMW_e
                    )
                    if useSMDBcheck.get() == 1:
                        search_spidermassDB(
                            SMresultfile,
                            monopeakvalue_search,
                            monopeaktolerance,
                            ionizationmode,
                            lowermasslimitMZ,
                            uppermasslimitMZ,
                        )
                    if useCScheck.get() == 1:
                        RESTrequest = chemspiderREST.IntrinsicPropSeachMonoMWREST(
                            parent_molecule_monopeakvalue_search,
                            monopeaktolerance,
                            CStimeout,
                        )
                        chemspider_decoder(
                            RESTrequest,
                            SMresultfile,
                            monopeakvalue_search,
                            monopeaktolerance,
                            ionizationmode,
                            parent_molecule_monopeakvalue_search,
                        )
                    if useDNcheck.get() == 1:
                        neutralmass = monopeakvalue_search - MMW_NH4 + MMW_e
                        de_novo_generator(
                            SMresultfile,
                            monopeakvalue_search,
                            ionizationmode,
                            neutralmass,
                            monopeaktolerance,
                        )

                if zerocheck.get() == 1:
                    ionizationmode = '+/- 0'
                    lowermasslimitMZ = monopeakvalue_search - monopeaktolerance
                    uppermasslimitMZ = monopeakvalue_search + monopeaktolerance
                    if useSMDBcheck.get() == 1:
                        search_spidermassDB(
                            SMresultfile,
                            monopeakvalue_search,
                            monopeaktolerance,
                            ionizationmode,
                            lowermasslimitMZ,
                            uppermasslimitMZ,
                        )
                    if useCScheck.get() == 1:
                        RESTrequest = chemspiderREST.IntrinsicPropSeachMonoMWREST(
                            monopeakvalue_search,
                            monopeaktolerance,
                            CStimeout,
                        )
                        chemspider_decoder(
                            RESTrequest,
                            SMresultfile,
                            monopeakvalue_search,
                            monopeaktolerance,
                            ionizationmode,
                            monopeakvalue_search,
                        )
                    if useDNcheck.get() == 1:
                        neutralmass = monopeakvalue_search
                        de_novo_generator(
                            SMresultfile,
                            monopeakvalue_search,
                            ionizationmode,
                            neutralmass,
                            monopeaktolerance,
                        )

                if minusHcheck.get() == 1:
                    ionizationmode = '[M-H]-'
                    parent_molecule_monopeakvalue_search = (
                        monopeakvalue_search + MMW_H - MMW_e
                    )

                    lowermasslimitMZ = (
                        monopeakvalue_search - monopeaktolerance + MMW_H - MMW_e
                    )
                    uppermasslimitMZ = (
                        monopeakvalue_search + monopeaktolerance + MMW_H - MMW_e
                    )
                    if useSMDBcheck.get() == 1:
                        search_spidermassDB(
                            SMresultfile,
                            monopeakvalue_search,
                            monopeaktolerance,
                            ionizationmode,
                            lowermasslimitMZ,
                            uppermasslimitMZ,
                        )
                    if useCScheck.get() == 1:
                        RESTrequest = chemspiderREST.IntrinsicPropSeachMonoMWREST(
                            parent_molecule_monopeakvalue_search,
                            monopeaktolerance,
                            CStimeout,
                        )
                        chemspider_decoder(
                            RESTrequest,
                            SMresultfile,
                            monopeakvalue_search,
                            monopeaktolerance,
                            ionizationmode,
                            parent_molecule_monopeakvalue_search,
                        )
                    if useDNcheck.get() == 1:
                        neutralmass = monopeakvalue_search + MMW_H - MMW_e
                        de_novo_generator(
                            SMresultfile,
                            monopeakvalue_search,
                            ionizationmode,
                            neutralmass,
                            monopeaktolerance,
                        )

        SMresultfile.close()
        tkMessageBox.showinfo(
            'SpiderMass DB search\nYou can find the results in SpiderMassResults.csv',
        )
        pf.close()

    # No mass list, single peak evaluation

    else:
        monopeakvalue_search = float(monopeakvalue_entry.get())
        monopeaktolerance = float(monopeaktolerance_entry.get())
        parent_molecule_monopeakvalue_search = monopeakvalue_search + MMW_e

        SMresultfile = open('SpiderMassResults.csv', 'w')
        SMresultwrite = str(
            'm/z'
            + ' +/- '
            + 'Mass Tolerance'
            + ';'
            + 'Ionization Mode'
            + ';'
            + 'Database'
            + ';'
            + 'Name'
            + ';'
            + 'ChemSpider ID'
            + ';'
            + 'Common Name (ChemSpider)'
            + ';'
            + 'Formula'
            + ';'
            + 'Monoisotopic Mass [Da]'
            + ';'
            + 'Mass Error [mDa]'
            + ';'
            + 'Isotope Distribution Fit (IDF)'
            + ';'
            + 'Spider Hit'
            + '\n',
        )
        SMresultfile.write(SMresultwrite)

        if hydrogencheck.get() == 1:
            ionizationmode = '[M+H]+'
            parent_molecule_monopeakvalue_search = monopeakvalue_search - MMW_H + MMW_e
            lowermasslimitMZ = monopeakvalue_search - monopeaktolerance - MMW_H + MMW_e
            uppermasslimitMZ = monopeakvalue_search + monopeaktolerance - MMW_H + MMW_e
            if useSMDBcheck.get() == 1:
                search_spidermassDB(
                    SMresultfile,
                    monopeakvalue_search,
                    monopeaktolerance,
                    ionizationmode,
                    lowermasslimitMZ,
                    uppermasslimitMZ,
                )
            if useCScheck.get() == 1:
                RESTrequest = chemspiderREST.IntrinsicPropSeachMonoMWREST(
                    parent_molecule_monopeakvalue_search, monopeaktolerance, CStimeout,
                )
                chemspider_decoder(
                    RESTrequest,
                    SMresultfile,
                    monopeakvalue_search,
                    monopeaktolerance,
                    ionizationmode,
                    parent_molecule_monopeakvalue_search,
                )
            if useDNcheck.get() == 1:
                neutralmass = monopeakvalue_search - MMW_H + MMW_e
                de_novo_generator(
                    SMresultfile,
                    monopeakvalue_search,
                    ionizationmode,
                    neutralmass,
                    monopeaktolerance,
                )

        if sodiumcheck.get() == 1:
            ionizationmode = '[M+Na]+'
            parent_molecule_monopeakvalue_search = monopeakvalue_search - MMW_Na + MMW_e
            lowermasslimitMZ = monopeakvalue_search - monopeaktolerance - MMW_Na + MMW_e
            uppermasslimitMZ = monopeakvalue_search + monopeaktolerance - MMW_Na + MMW_e
            if useSMDBcheck.get() == 1:
                search_spidermassDB(
                    SMresultfile,
                    monopeakvalue_search,
                    monopeaktolerance,
                    ionizationmode,
                    lowermasslimitMZ,
                    uppermasslimitMZ,
                )
            if useCScheck.get() == 1:
                RESTrequest = chemspiderREST.IntrinsicPropSeachMonoMWREST(
                    parent_molecule_monopeakvalue_search,
                    monopeaktolerance,
                    CStimeout,
                )
                chemspider_decoder(
                    RESTrequest,
                    SMresultfile,
                    monopeakvalue_search,
                    monopeaktolerance,
                    ionizationmode,
                    parent_molecule_monopeakvalue_search,
                )
            if useDNcheck.get() == 1:
                neutralmass = monopeakvalue_search - MMW_Na + MMW_e
                de_novo_generator(
                    SMresultfile,
                    monopeakvalue_search,
                    ionizationmode,
                    neutralmass,
                    monopeaktolerance,
                )

        if potassiumcheck.get() == 1:
            ionizationmode = '[M+K]+'
            parent_molecule_monopeakvalue_search = monopeakvalue_search - MMW_K + MMW_e
            lowermasslimitMZ = monopeakvalue_search - monopeaktolerance - MMW_K + MMW_e
            uppermasslimitMZ = monopeakvalue_search + monopeaktolerance - MMW_K + MMW_e
            if useSMDBcheck.get() == 1:
                search_spidermassDB(
                    SMresultfile,
                    monopeakvalue_search,
                    monopeaktolerance,
                    ionizationmode,
                    lowermasslimitMZ,
                    uppermasslimitMZ,
                )
            if useCScheck.get() == 1:
                RESTrequest = chemspiderREST.IntrinsicPropSeachMonoMWREST(
                    parent_molecule_monopeakvalue_search,
                    monopeaktolerance,
                    CStimeout,
                )
                chemspider_decoder(
                    RESTrequest,
                    SMresultfile,
                    monopeakvalue_search,
                    monopeaktolerance,
                    ionizationmode,
                    parent_molecule_monopeakvalue_search,
                )
            if useDNcheck.get() == 1:
                neutralmass = monopeakvalue_search - MMW_K + MMW_e
                de_novo_generator(
                    SMresultfile,
                    monopeakvalue_search,
                    ionizationmode,
                    neutralmass,
                    monopeaktolerance,
                )

        if ammoniumcheck.get() == 1:
            ionizationmode = '[M+NH4]+'
            parent_molecule_monopeakvalue_search = (
                monopeakvalue_search - MMW_NH4 + MMW_e
            )

            lowermasslimitMZ = (
                monopeakvalue_search - monopeaktolerance - MMW_NH4 + MMW_e
            )
            uppermasslimitMZ = (
                monopeakvalue_search + monopeaktolerance - MMW_NH4 + MMW_e
            )
            if useSMDBcheck.get() == 1:
                search_spidermassDB(
                    SMresultfile,
                    monopeakvalue_search,
                    monopeaktolerance,
                    ionizationmode,
                    lowermasslimitMZ,
                    uppermasslimitMZ,
                )
            if useCScheck.get() == 1:
                RESTrequest = chemspiderREST.IntrinsicPropSeachMonoMWREST(
                    parent_molecule_monopeakvalue_search,
                    monopeaktolerance,
                    CStimeout,
                )
                chemspider_decoder(
                    RESTrequest,
                    SMresultfile,
                    monopeakvalue_search,
                    monopeaktolerance,
                    ionizationmode,
                    parent_molecule_monopeakvalue_search,
                )
            if useDNcheck.get() == 1:
                neutralmass = monopeakvalue_search - MMW_NH4 + MMW_e
                de_novo_generator(
                    SMresultfile,
                    monopeakvalue_search,
                    ionizationmode,
                    neutralmass,
                    monopeaktolerance,
                )

        if zerocheck.get() == 1:
            ionizationmode = '+/- 0'
            lowermasslimitMZ = monopeakvalue_search - monopeaktolerance
            uppermasslimitMZ = monopeakvalue_search + monopeaktolerance
            if useSMDBcheck.get() == 1:
                search_spidermassDB(
                    SMresultfile,
                    monopeakvalue_search,
                    monopeaktolerance,
                    ionizationmode,
                    lowermasslimitMZ,
                    uppermasslimitMZ,
                )
            if useCScheck.get() == 1:
                RESTrequest = chemspiderREST.IntrinsicPropSeachMonoMWREST(
                    monopeakvalue_search,
                    monopeaktolerance,
                    CStimeout,
                )
                chemspider_decoder(
                    RESTrequest,
                    SMresultfile,
                    monopeakvalue_search,
                    monopeaktolerance,
                    ionizationmode,
                    monopeakvalue_search,
                )
            if useDNcheck.get() == 1:
                neutralmass = monopeakvalue_search
                de_novo_generator(
                    SMresultfile,
                    monopeakvalue_search,
                    ionizationmode,
                    neutralmass,
                    monopeaktolerance,
                )

        if minusHcheck.get() == 1:
            ionizationmode = '[M-H]-'
            parent_molecule_monopeakvalue_search = monopeakvalue_search + MMW_H - MMW_e

            lowermasslimitMZ = monopeakvalue_search - monopeaktolerance + MMW_H - MMW_e
            uppermasslimitMZ = monopeakvalue_search + monopeaktolerance + MMW_H - MMW_e
            if useSMDBcheck.get() == 1:
                search_spidermassDB(
                    SMresultfile,
                    monopeakvalue_search,
                    monopeaktolerance,
                    ionizationmode,
                    lowermasslimitMZ,
                    uppermasslimitMZ,
                )
            if useCScheck.get() == 1:
                RESTrequest = chemspiderREST.IntrinsicPropSeachMonoMWREST(
                    parent_molecule_monopeakvalue_search,
                    monopeaktolerance,
                    CStimeout,
                )
                chemspider_decoder(
                    RESTrequest,
                    SMresultfile,
                    monopeakvalue_search,
                    monopeaktolerance,
                    ionizationmode,
                    parent_molecule_monopeakvalue_search,
                )
            if useDNcheck.get() == 1:
                neutralmass = monopeakvalue_search + MMW_H - MMW_e
                de_novo_generator(
                    SMresultfile,
                    monopeakvalue_search,
                    ionizationmode,
                    neutralmass,
                    monopeaktolerance,
                )

        SMresultfile.close()
        tkMessageBox.showinfo(
            'SpiderMass DB search', 'You can find the results in SpiderMassResults.csv',
        )


def chemspider_decoder(
    RESTrequestInt,
    SMresultfile,
    monopeakvalue_search,
    monopeaktolerance,
    ionizationmode,
    parent_molecule_monopeakvalue_search,
):
    import chemspidermasspy

    print(f'RESTrequestInt: {RESTrequestInt}')
    CSmaxHits = CSmaxHits_set.get()

    if RESTrequestInt == ['empty']:
        print('No result from ChemSpider DB search!')

    else:
        try:
            HitNumber = len(RESTrequestInt)

            print(
                f'{HitNumber} results found in ChemSpider DB, importing the first {CSmaxHits} hits',
            )
            RESTrequestInt = RESTrequestInt[:CSmaxHits]
            print(f'sliced RESTrequestInt: {RESTrequestInt}')
            for CSID in RESTrequestInt:
                CSID = str(CSID)
                print(CSID)

                chemspiderCompoundID = chemspidermasspy.Compound(CSID)
                print('generated compound')

                chemspiderName = chemspiderCompoundID.commonname
                print(f'cs Name = {chemspiderName}, {type(chemspiderName)}')
                chemspiderNameString = str(chemspiderName)
                chemspiderNameString = re.sub(';', ',', chemspiderNameString)
                chemspiderNameString = re.sub('\n', ' ', chemspiderNameString)

                chemspiderMonoMass = chemspiderCompoundID.monoisotopicmass
                chemspiderMonoMassString = str(chemspiderMonoMass)

                chemspiderSumFormula = chemspiderCompoundID.mf
                chemspiderSumFormulaString = str(chemspiderSumFormula)
                Formula = re.sub(
                    r'[^a-zA-Z0-9\n\.]', '',
                    chemspiderSumFormulaString,
                )

                IDFvalue = 'n.a.'
                SpiderHit = 'n.a.'

                masserror = 1000 * (
                    parent_molecule_monopeakvalue_search - chemspiderMonoMass
                )
                if useIDFcheck:
                    IDFvalue = IDF_checker(Formula, ionizationmode)
                    SpiderHit = math.pow(IDFvalue * masserror, 2)
                    IDFvalue = str('%f' % IDFvalue)
                    SpiderHit = str('%.12f' % SpiderHit)

                saveString = f"""{monopeakvalue_search} +/- {monopeaktolerance};{ionizationmode};ChemSpider DB (online);{chemspiderNameString};{CSID};{chemspiderNameString};{Formula};{chemspiderMonoMassString};{masserror};{IDFvalue};{SpiderHit}\n"""

                try:
                    SMresultfile.write(saveString)
                except:
                    print(f'\n{saveString}, caused an error!\n')

        except:
            print('No result from ChemSpider DB search!')


def search_spidermassDB(
    SMresultfile,
    monopeakvalue_search,
    monopeaktolerance,
    ionizationmode,
    lowermasslimitMZ,
    uppermasslimitMZ,
):

    # load SpiderMassDB
    spidermassdb = SpiderMassDBfilename_value.get()
    smDB = np.genfromtxt(
        spidermassdb, delimiter=';', names=True, dtype=None, encoding='utf-8',
    )
    numrows = len(smDB)
    STsmDB = smDB['Search_Term']
    CSIDsmDB = smDB['ChemSpiderID']
    CNsmDB = smDB['Common_Name']
    FsmDB = smDB['Formula']
    MMsmDB = smDB['Monoisotopic_Mass']
    spidermassdbString = ntpath.basename(spidermassdb)

    print(
        f'Search for {monopeakvalue_search} \u00b1 {monopeaktolerance} {ionizationmode} in {spidermassdb}',
    )
    for i in range(numrows):
        if lowermasslimitMZ <= MMsmDB[i] <= uppermasslimitMZ:
            ST = STsmDB[i]
            try:
                ST = ST.decode('ascii')
            except:
                ST = 'decoding error'
            CSID = str(CSIDsmDB[i])
            MM = str(MMsmDB[i])
            CN = CNsmDB[i]
            try:
                CN = CN.decode('ascii')
            except:
                CN = 'decoding error'
            Formula = FsmDB[i]
            Formula = Formula.decode('ascii')

            IDFvalue = 'n.a.'
            SpiderHit = 'n.a.'
            masserror = 1000 * \
                (((lowermasslimitMZ + uppermasslimitMZ) / 2) - MMsmDB[i])
            if queryParameter['useIDFcheck'] == 1:
                IDFvalue = IDF_checker(Formula, ionizationmode)
                SpiderHit = math.pow(IDFvalue * masserror, 2)
                IDFvalue = str('%f' % IDFvalue)
                SpiderHit = str('%.12f' % SpiderHit)

            SMresultwrite = f"""{monopeakvalue_search} +/- {monopeaktolerance};
                {ionizationmode};{spidermassdbString};{ST};
                {CSID};{CN};{Formula};{MM};
                {masserror}; {IDFvalue}; {SpiderHit}\n"""
            SMresultfile.write(SMresultwrite)


def select_spidermassdb():
    dbfilename = askopenfilename()
    SpiderMassDBfilename_value.set(dbfilename)


def select_MZpeaklist():
    plfilename = askopenfilename()
    MZpeaklistfilename_value.set(plfilename)


def de_novo_generator(
    SMresultfile, monopeakvalue_search, ionizationmode, neutralmass, monopeaktolerance,
):
    neutralmass = int(neutralmass)
    print(f'De Novo Generator \n, {type(neutralmass)}')

    maxcountC = (neutralmass / 12) + 1
    if switchC.get() == 1:
        print(f'Max. C: {maxcountC}')
        switchChr2 = f'-C 0-{maxcountC}'

    else:
        switchChr2 = '-C 0-0'

    if switch13C.get() == 1:
        maxcount13C = (neutralmass / 13) + 1  # No 12C
        print(f'Max. 13C: {maxcount13C}')
        switch13Chr2 = str('-1 0-' + str(maxcount13C))

    else:
        switch13Chr2 = '-1 0-0'

    if switchH.get() == 1:
        # assuming CH molecule with 4H/C
        maxcountH = (4 * neutralmass / 16) + 1
        print(f'Max. H: {maxcountH}')
        switchHhr2 = f'-H 0-{maxcountH}'

    else:
        switchHhr2 = '-H 0-0'

    if switchD.get() == 1:
        # same ratio as for H: 4D/C but MW(D)=2
        maxcountD = (4 * neutralmass / 20) + 1
        print(f'Max. D: {maxcountD}')
        switchDhr2 = str('-D 0-' + str(maxcountD))

    else:
        switchDhr2 = '-D 0-0'

    if switchN.get() == 1:
        maxcountN = int((2 * maxcountC / 3) + 1)  # for 2N/C
        print('Max. N: ', str(maxcountN))
        switchNhr2 = str('-N 0-' + str(maxcountN))

    else:
        switchNhr2 = '-N 0-0'

    if switch15N.get() == 1:
        maxcount15N = int((2 * maxcountC / 3) + 1)  # same as N, but MW(15N)=15
        print('Max. N: ', str(maxcount15N))
        switch15Nhr2 = str('-M 0-' + str(maxcount15N))

    else:
        switch15Nhr2 = '-M 0-0'

    if switchO.get() == 1:
        maxcountO = int((2 * maxcountC / 3) + 1)  # for 2O/C
        print('Max. O: ', str(maxcountO))
        switchOhr2 = str('-O 0-' + str(maxcountO))

    else:
        switchOhr2 = '-O 0-0'

    if switchF.get() == 1:
        maxcountF = int((2 * maxcountC / 3) + 1)  # for 2F/C
        print('Max. F: ', str(maxcountF))
        switchFhr2 = str('-F 0-' + str(maxcountF))

    else:
        switchFhr2 = '-F 0-0'

    if switchNa.get() == 1:
        maxcountNa = int((maxcountC / 2) + 1)  # for 1Na/C
        print('Max. Na: ', str(maxcountNa))
        switchNahr2 = str('-A 0-' + str(maxcountNa))

    else:
        switchNahr2 = '-A 0-0'

    if switchSi.get() == 1:
        maxcountSi = int((maxcountC / 2) + 1)  # for 1Si/C
        print('Max. Si: ', str(maxcountSi))
        switchSihr2 = str('-A 0-' + str(maxcountSi))

    else:
        switchSihr2 = '-I 0-0'

    if switchP.get() == 1:
        maxcountP = int((maxcountC / 2) + 1)  # for 1P/C
        print('Max. P: ', str(maxcountP))
        switchPhr2 = str('-P 0-' + str(maxcountP))

    else:
        switchPhr2 = '-P 0-0'

    if switchS.get() == 1:
        maxcountS = int((maxcountC / 2) + 1)  # for 1S/C
        print('Max. S: ', str(maxcountS))
        switchShr2 = str('-S 0-' + str(maxcountS))

    else:
        switchShr2 = '-S 0-0'

    if switchCl.get() == 1:
        maxcountCl = int((maxcountC / 2) + 1)  # for 1Cl/C
        print('Max. Cl: ', str(maxcountCl))
        switchClhr2 = str('-L 0-' + str(maxcountCl))

    else:
        switchClhr2 = '-L 0-0'

    if switchBr.get() == 1:
        maxcountBr = int((maxcountC / 2) + 1)  # for 1Br/C
        print('Max. Br: ', str(maxcountBr))
        switchBrhr2 = str('-B 0-' + str(maxcountBr))

    else:
        switchBrhr2 = '-B 0-0'

    start = time.time()
    HR2mmass = str(neutralmass)
    HR2monopeaktolerance = f'{1000 * monopeaktolerance}'
    HR2options = f'-m {HR2mmass} -t {HR2monopeaktolerance}'
    # print(HR2options)

    # Call external program, depending on operating system

    if sys.platform == 'linux' or sys.platform == 'linux2':
        call(
            [
                './smformula-linux',
                '-m ' + HR2mmass,
                '-t ' + HR2monopeaktolerance,
                switchChr2,
                switch13Chr2,
                switchHhr2,
                switchDhr2,
                switchNhr2,
                switch15Nhr2,
                switchOhr2,
                switchFhr2,
                switchNahr2,
                switchSihr2,
                switchPhr2,
                switchShr2,
                switchClhr2,
                switchBrhr2,
            ],
        )

    if sys.platform == 'win32':
        call(
            [
                'smformula-win.exe',
                '-m ' + HR2mmass,
                '-t ' + HR2monopeaktolerance,
                switchChr2,
                switch13Chr2,
                switchHhr2,
                switchDhr2,
                switchNhr2,
                switch15Nhr2,
                switchOhr2,
                switchFhr2,
                switchNahr2,
                switchSihr2,
                switchPhr2,
                switchShr2,
                switchClhr2,
                switchBrhr2,
            ],
        )

    elapsed = time.time() - start
    print(f'time elapsed for formula generation [s]: {elapsed}')
    print('importing results..')


# dnDB = np.genfromtxt(
#     "HR3.csv", delimiter=";", names=True, dtype=None, encoding="utf-8"
# )


with open('HR3.csv', newline='\n') as csvfile:
    entries = csv.DictReader(csvfile, delimiter=';')
    entries = [dict(entry) for entry in entries]

for entry in entries:
    entry['IDFvalue'] = 'n.a.'
    entry['SpiderHit'] = 'n.a.'
    print(entry)
    if queryParameter['useIDFcheck'] == 1:
        IDFvalue = int(IDF_checker(entry['Formula'], ionizationmode))
        entry['IDFvalue'] = f'{IDFvalue:.6f}'
        SpiderHit = math.pow(IDFvalue * int(float(entry['Mass_Error_mDa'])), 2)
        entry['SpiderHit'] = f'{SpiderHit:.12f}'
        saveString = (
            str(monopeakvalue_search)
            + '+/-'
            + str(monopeaktolerance)
            + ';'
            + ionizationmode
            + ';'
            + 'HR3.csv'
            + ';'
            + 'De Novo Formula'
            + ';'
            + 'n.a.'
            + ';'
            + 'n.a.'
            + ';'
            + entry['Formula']
            + ';'
            + entry['Mass_Da']
            + ';'
            + entry['Mass_Error_mDa']
            + ';'
            + entry['IDFvalue']
            + ';'
            + entry['SpiderHit']
            + '\n'
        )
        SMresultfile.write(saveString)


def IDF_checker(Formula, ionizationmode):

    # Special SpiderMass Feature: The Formula for the MS ION is calculated, and subsequently its isotope pattern
    print('\n')
    print('Ionization mode: ', ionizationmode)
    print('Isotope Distribution for Formula ', Formula)
    Splitter = re.findall(r'\d+|\D+', Formula)
    # print("Formula Splitter : ", Splitter)

    if ionizationmode == '[M+H]+':
        try:
            Hindex = Splitter.index('H')
            # print("H index: ", Hindex, "\n")
            HindexCount = Hindex + 1
            Splitter[HindexCount] = 1 + int(Splitter[HindexCount])
        except:
            Splitter.append('H')

    if ionizationmode == '[M+K]+':
        try:
            Kindex = Splitter.index('K')
            # print("K index: ", Kindex, "\n")
            KindexCount = Kindex + 1
            Splitter[KindexCount] = 1 + int(Splitter[KindexCount])
        except:
            Splitter.append('K')

    if ionizationmode == '[M+Na]+':
        try:
            Naindex = Splitter.index('Na')
            # print("Na index: ", Naindex, "\n")
            NaindexCount = Naindex + 1
            Splitter[NaindexCount] = 1 + int(Splitter[NaindexCount])
        except:
            Splitter.append('Na')

    if ionizationmode == '[M+NH4]+':
        try:
            Nindex = Splitter.index('N')
            # print("N index: ", Nindex, "\n")
            NindexCount = Nindex + 1
            Splitter[NindexCount] = 1 + int(Splitter[NindexCount])
        except:
            Splitter.append('N')
        try:
            Hindex = Splitter.index('H')
            # print("H index: ", Hindex, "\n")
            HindexCount = Hindex + 1
            Splitter[HindexCount] = 4 + int(Splitter[HindexCount])
        except:
            Splitter.append('H')
            Splitter.append('4')

    if ionizationmode == '[M-H]-':
        try:
            Hindex = Splitter.index('H')
            # print("H index: ", Hindex, "\n")
            HindexCount = Hindex + 1
            Splitter[HindexCount] = int(Splitter[HindexCount]) - 1
        except:
            print(
                'No H in putative formula: This is not a possible for ionization mode [M-H]-! \n',
            )

    # print("Formula Splitter modified: ", Splitter)
    Formula = ''.join(map(str, Splitter))
    print('Isotope Distribution calculated for Formula of ION: ', Formula, '\n')

    # Call external program, depending on operating system

    if sys.platform == 'linux' or sys.platform == 'linux2':
        call(['./smisotope-linux', '-f', Formula])

    if sys.platform == 'win32':
        call(['smisotope-win.exe', '-f', Formula])

    isoDB = np.genfromtxt(
        'isotopes.csv', delimiter=';', names=True, dtype=None, encoding='utf-8',
    )
    TM0 = isoDB['TM0']
    TM1 = isoDB['TM1']
    TM2 = isoDB['TM2']
    TM3 = isoDB['TM3']
    TMarray = np.array([TM0, TM1, TM2, TM3])
    TMarray = TMarray / TMarray.max()  # normalize the vector
    print('Theoretical Isotope M  : ', TM0)
    print('Theoretical Isotope M+1: ', TM1)
    print('Theoretical Isotope M+2: ', TM2)
    print('Theoretical Isotope M+3: ', TM3, '\n')
    print('Theoretical Isotopes (normalized): ', TMarray)
    EM0 = isoabundance1_set.get()
    EM1 = isoabundance2_set.get()
    EM2 = isoabundance3_set.get()
    EM3 = isoabundance4_set.get()
    EMarray = np.array([EM0, EM1, EM2, EM3])
    EMarray = EMarray / EMarray.max()  # normalize the vector
    print('Experimental Isotopes (normalized): ', EMarray)
    IDF = EMarray - TMarray
    # print(IDF)
    IDF = np.abs(IDF)
    # print(IDF)
    IDF = np.sum(IDF)
    print('Isotope Distribution Fit (IDF): ', IDF, '\n')
    return IDF


def Quit_SpiderMass():
    quit()


# Definition of global variables

# Main program

# Main Window

root = Tk()
root.title('SpiderMass Identifier 1.1')

# GUI

features = Frame(root)
features.grid(sticky=W)

# INPUT area

header_label = Label(features, text='INPUT (Single Peak, or list):')
header_label.grid(row=3, column=1, sticky=W)

# m/z read from the spectrum
monopeak_label = Label(features, text='Mono peak M [m/z]')
monopeak_label.grid(row=1, column=2, sticky=W)

monopeak_set = DoubleVar()
monopeak_set.set(323.0196)

monopeakvalue_entry = Entry(features, textvariable=monopeak_set)
monopeakvalue_entry.grid(row=3, column=2, sticky=W)

monopeaktolerance_label = Label(
    features, text='Mass error [m/z]',
)  # mass accuracy
monopeaktolerance_label.grid(row=1, column=3, sticky=W)

monotolerance_set = DoubleVar()
monotolerance_set.set(0.001)

monopeaktolerance_entry = Entry(features, textvariable=monotolerance_set)
monopeaktolerance_entry.grid(row=3, column=3, sticky=W)

denovo_label = Label(features, text='For IDF ->')  # Isotope
denovo_label.grid(row=3, column=4, sticky=W)

isotopeabundance_label = Label(
    features, text='Isotope abundance:',
)  # Isotope abunance
isotopeabundance_label.grid(row=3, column=5, sticky=W)

isotopeabundance_label1 = Label(features, text='M [% or absolute]')
isotopeabundance_label1.grid(row=1, column=6, sticky=W)

isotopeabundance_label2 = Label(features, text='M+1 [% or absolute]')
isotopeabundance_label2.grid(row=1, column=7, sticky=W)

isotopeabundance_label3 = Label(features, text='M+2 [% or absolute]')
isotopeabundance_label3.grid(row=1, column=8, sticky=W)

isotopeabundance_label4 = Label(features, text='M+3 [% or absolute]')
isotopeabundance_label4.grid(row=1, column=9, sticky=W)

isoabundance1_set = DoubleVar()
isoabundance1_set.set(100)

isotopeabundance_entry1 = Entry(features, textvariable=isoabundance1_set)
isotopeabundance_entry1.grid(row=3, column=6, sticky=W)

isoabundance2_set = DoubleVar()
isoabundance2_set.set(13)

isotopeabundance_entry2 = Entry(features, textvariable=isoabundance2_set)
isotopeabundance_entry2.grid(row=3, column=7, sticky=W)

isoabundance3_set = DoubleVar()
isoabundance3_set.set(66)

isotopeabundance_entry3 = Entry(features, textvariable=isoabundance3_set)
isotopeabundance_entry3.grid(row=3, column=8, sticky=W)

isoabundance4_set = DoubleVar()
isoabundance4_set.set(8)

isotopeabundance_entry4 = Entry(features, textvariable=isoabundance4_set)
isotopeabundance_entry4.grid(row=3, column=9, sticky=W)

# Peak list selection

action_button = Button(
    features, text='Select m/z peaklist', command=select_MZpeaklist,
)
action_button.grid(row=4, column=1)

MZpeaklistfilename_value = StringVar()
MZpeaklistfilename_output = Entry(
    features, textvariable=MZpeaklistfilename_value, width=160,
)
MZpeaklistfilename_output.grid(row=4, column=2, columnspan=8)


# Ion checkboxes

ions_label = Label(features, text='   Ion(s):')
ions_label.grid(row=5, column=1, sticky=E)

hydrogencheck = IntVar()
hydrogencheckbox = Checkbutton(
    features, text='[M+H]+', variable=hydrogencheck, onvalue=1, offvalue=0, height=2,
)
hydrogencheckbox.grid(row=5, column=2, sticky=W)
hydrogencheckbox.select()

sodiumcheck = IntVar()
sodiumcheckbox = Checkbutton(
    features, text='[M+Na]+', variable=sodiumcheck, onvalue=1, offvalue=0, height=2,
)
sodiumcheckbox.grid(row=5, column=3, sticky=W)
sodiumcheckbox.select()

potassiumcheck = IntVar()
potassiumcheckbox = Checkbutton(
    features, text='[M+K]+', variable=potassiumcheck, onvalue=1, offvalue=0, height=2,
)
potassiumcheckbox.grid(row=5, column=4, sticky=W)
# potassiumcheckbox.select()

ammoniumcheck = IntVar()
ammoniumcheckbox = Checkbutton(
    features, text='[M+NH4]+', variable=ammoniumcheck, onvalue=1, offvalue=0, height=2,
)
ammoniumcheckbox.grid(row=5, column=5, sticky=W)
# ammoniumcheckbox.select()

zerocheck = IntVar()
zerocheckbox = Checkbutton(
    features, text='+/- 0', variable=zerocheck, onvalue=1, offvalue=0, height=2,
)
zerocheckbox.grid(row=5, column=6, sticky=W)
# zerocheckbox.select()

minusHcheck = IntVar()
minusHcheckbox = Checkbutton(
    features, text='[M-H]-', variable=minusHcheck, onvalue=1, offvalue=0, height=2,
)
minusHcheckbox.grid(row=5, column=7, sticky=W)
# minusHcheckbox.select()

# SPIDERMASS MODULE

useSMDBcheck = IntVar()
useSMDBcheckbox = Checkbutton(
    features,
    text='SPIDERMASS DATABASE SEARCH',
    variable=useSMDBcheck,
    onvalue=1,
    offvalue=0,
    height=2,
)
useSMDBcheckbox.grid(row=6, column=1, sticky=W)
# useSMDBcheckbox.select()

# SpidermassDB selection

action_button = Button(
    features, text='Select SpidermassDB', command=select_spidermassdb,
)
action_button.grid(row=7, column=1)

SpiderMassDBfilename_value = StringVar()
SpiderMassDBfilename_value.set('./dbase/streptomyces_meta_SpiderMassDB.csv')
SpiderMassDBfilename_output = Entry(
    features, textvariable=SpiderMassDBfilename_value, width=160,
)
SpiderMassDBfilename_output.grid(row=7, column=2, columnspan=8)


# CHEMSPIDER MODULE

useCScheck = IntVar()
useCScheckbox = Checkbutton(
    features,
    text='CHEMSPIDER.COM SEARCH',
    variable=useCScheck,
    onvalue=1,
    offvalue=0,
    height=2,
)
useCScheckbox.grid(row=9, column=1, sticky=W)
# useCScheckbox.select()

# set timeout for ChemSpider.com Online Search
CStimeout_label = Label(features, text='timeout [s]:')
CStimeout_label.grid(row=9, column=2, sticky=W)

CStimeout_set = IntVar()
CStimeout_set.set(3)

CStimeoutvalue_entry = Entry(features, textvariable=CStimeout_set)
CStimeoutvalue_entry.grid(row=9, column=3, sticky=W)

# Set max. hits reported from ChemSpider.com online search

CSmaxHits_label = Label(features, text='max. hits:')
CSmaxHits_label.grid(row=9, column=4, sticky=W)

CSmaxHits_set = IntVar()
CSmaxHits_set.set(10)

CSmaxHitsvalue_entry = Entry(features, textvariable=CSmaxHits_set)
CSmaxHitsvalue_entry.grid(row=9, column=5, sticky=W)
# De-Novo MODULE

useDNcheck = IntVar()
useDNcheckbox = Checkbutton(
    features,
    text='DE-NOVO FORMULA BUILDER',
    variable=useDNcheck,
    onvalue=1,
    offvalue=0,
    height=2,
)
useDNcheckbox.grid(row=10, column=1, sticky=W)
useDNcheckbox.select()

DNelements_label = Label(features, text='   Elements for De-Novo:')
DNelements_label.grid(row=11, column=1, sticky=E)

switchC = IntVar()
switchCcheckbox = Checkbutton(
    features, text='C', variable=switchC, onvalue=1, offvalue=0, height=2,
)
switchCcheckbox.grid(row=11, column=2, sticky=W)
switchCcheckbox.select()

switch13C = IntVar()
switch13Ccheckbox = Checkbutton(
    features, text='13C', variable=switch13C, onvalue=1, offvalue=0, height=2,
)
switch13Ccheckbox.grid(row=11, column=3, sticky=W)
# switch13Ccheckbox.select()

switchH = IntVar()
switchHcheckbox = Checkbutton(
    features, text='H', variable=switchH, onvalue=1, offvalue=0, height=2,
)
switchHcheckbox.grid(row=11, column=4, sticky=W)
switchHcheckbox.select()

switchD = IntVar()
switchDcheckbox = Checkbutton(
    features, text='D', variable=switchD, onvalue=1, offvalue=0, height=2,
)
switchDcheckbox.grid(row=11, column=5, sticky=W)
# switchDcheckbox.select()

switchN = IntVar()
switchNcheckbox = Checkbutton(
    features, text='N', variable=switchN, onvalue=1, offvalue=0, height=2,
)
switchNcheckbox.grid(row=11, column=6, sticky=W)
switchNcheckbox.select()

switch15N = IntVar()
switch15Ncheckbox = Checkbutton(
    features, text='15N', variable=switch15N, onvalue=1, offvalue=0, height=2,
)
switch15Ncheckbox.grid(row=11, column=7, sticky=W)
# switch15Ncheckbox.select()

switchO = IntVar()
switchOcheckbox = Checkbutton(
    features, text='O', variable=switchO, onvalue=1, offvalue=0, height=2,
)
switchOcheckbox.grid(row=11, column=8, sticky=W)
switchOcheckbox.select()

switchF = IntVar()
switchFcheckbox = Checkbutton(
    features, text='F', variable=switchF, onvalue=1, offvalue=0, height=2,
)
switchFcheckbox.grid(row=12, column=2, sticky=W)
# switchFcheckbox.select()

switchNa = IntVar()
switchNacheckbox = Checkbutton(
    features, text='Na', variable=switchNa, onvalue=1, offvalue=0, height=2,
)
switchNacheckbox.grid(row=12, column=3, sticky=W)
# switchNacheckbox.select()

switchSi = IntVar()
switchSicheckbox = Checkbutton(
    features, text='Si', variable=switchSi, onvalue=1, offvalue=0, height=2,
)
switchSicheckbox.grid(row=12, column=4, sticky=W)
# switchSicheckbox.select()

switchP = IntVar()
switchPcheckbox = Checkbutton(
    features, text='P', variable=switchP, onvalue=1, offvalue=0, height=2,
)
switchPcheckbox.grid(row=12, column=5, sticky=W)
# switchPcheckbox.select()

switchS = IntVar()
switchScheckbox = Checkbutton(
    features, text='S', variable=switchS, onvalue=1, offvalue=0, height=2,
)
switchScheckbox.grid(row=12, column=6, sticky=W)
# switchScheckbox.select()

switchCl = IntVar()
switchClcheckbox = Checkbutton(
    features, text='Cl', variable=switchCl, onvalue=1, offvalue=0, height=2,
)
switchClcheckbox.grid(row=12, column=7, sticky=W)
switchClcheckbox.select()

switchBr = IntVar()
switchBrcheckbox = Checkbutton(
    features, text='Br', variable=switchBr, onvalue=1, offvalue=0, height=2,
)
switchBrcheckbox.grid(row=12, column=8, sticky=W)
# switchBrcheckbox.select()

# ISOTOPE DISTRIBUTION FIT
useIDFcheck = IntVar()
useIDFcheckbox = Checkbutton(
    features,
    text='ISOTOPE DISTRIBUTION FIT (IDF)',
    variable=useIDFcheck,
    onvalue=1,
    offvalue=0,
    height=2,
)
useIDFcheckbox.grid(row=13, column=1, sticky=W)
useIDFcheckbox.select()

Empty_label = Label(features, text='   ')
Empty_label.grid(row=79, column=1, sticky=E)

# Action Buttons
action_button = Button(
    features, text='Start SpiderMass ID', command=ion_search,
)
action_button.grid(row=80, column=1)

action_button = Button(features, text='Quit', command=Quit_SpiderMass)
action_button.grid(row=80, column=9)

# creating a menu

menu = Menu(root)
root.config(menu=menu)

filemenu = Menu(menu)
menu.add_cascade(label='File', menu=filemenu)
filemenu.add_command(label='Exit', command=Quit_SpiderMass)

spidermenu = Menu(menu)
menu.add_cascade(label='SpiderMass Database', menu=spidermenu)
spidermenu.add_command(label='DB Generator', command=start_dbgenerator)

optionmenu = Menu(menu)
menu.add_cascade(label='Options', menu=optionmenu)
optionmenu.add_command(label='Check Operating System', command=check_OS)

helpmenu = Menu(menu)
menu.add_cascade(label='Help', menu=helpmenu)
helpmenu.add_command(label='Usage', command=help_usage)
helpmenu.add_command(label='About...', command=help_about)

root.mainloop()
