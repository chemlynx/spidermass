README to HR, 20050617
======================

NAME

hr - calculates elemental compositions for a given mass


SYNOPSIS

hr [-h] [-t tolerance_mmu] [-m mass] [-c comment]
   [-X min-max] [-n|-p] datafile


DESCRIPTION

'hr' is a simple program to perform a common task in mass spectrometry: It
calculates the possible elemental compositions that fit a given mass. In
contrast to most programs that I came across, this one is not an
"integrated" application but a small stand-alone utility. It allows to run
calculations in batch mode, via the command line, or using a web interface.

However, if you are not working with mass spectrometry, if you are neither
chemist nor physicist, then this program is probably not for you ;-)


OPTIONS

    -h      shows a short Help screen.
    -v      Display version information.
    -t tol  Set tolerance to 'tol' mmu (default is 5 mmu).
    -m mz   Set mass to 'mz'.
    -c txt  Set comment to 'txt' (only useful together with '-m').
    -p      Positive ions; electron mass is removed from the formula.
    -n      Negative ions; electron mass is added to the formula.
    -X a-b  For element X, use atom range a to b as follows:

    ---------------------------------------------------------------
    X    key    mass (internal precision is higher than shown)
    ---------------------------------------------------------------
    C     -C       12.000000
    13C     -1       13.003355
    H     -H        1.007825
    D     -D        2.014102
    N     -N       14.003074
    O     -O       15.994915
    F     -F       18.998403
    Na     -A       22.989770
    Si     -I       27.976927
    P     -P       30.973762
    S     -S       31.972071
    Cl     -L       34.968853
    Br     -B       78.918338
    ---------------------------------------------------------------

    Notes:

    - All other arguments passed to the program are files to read from
    (but only the first file will be read).

    - There is no possibility to define another element 'on the fly'.

    - All keycodes and options are case sensitive.


DEFAULTS

    Composition limits  C:0-30 H:0-50 N:0-10 O:0-5
    Mass tolerance      +/- 5 mmu around entered mass
    Charge              neutral


LIMITS

Compositions with RDB ("Rings and Double Bond Equivalents") below -0.5
are not shown ("chemical nonsense"). If you want to display them,
comment out the "if"-clause around line 395 and recompile.

Calculation time increases linearly with the number of atoms (... you
expected that, didn't you?).

Atom 'counters' are signed integers, so a maximum of 32767 atoms per
symbol should be possible. However, this may well be beyond a "reasonable"
use for high-resolution MS ;-)

Atomic masses are as published by IUPAC, 2002-10-02.


OUTPUT

The result can be redirected to file and comprises:

    - the compositional limits,
    - the tolerance used (mmu),
    - the measured mass (entered by the user),
    - the charge (+1, neutral, -1),
    - and a list of:
        the matching compositions,
        their rings & double bond equivalents,
        the calculated mass
        and the deviation 'measured minus calculated mass'


EXIT CODE

Exit code is    0 if program execution was successful,
                1 if any error occurred.


BATCH MODE

The program can be run in batch mode, simply by passing the name of a
'data file' on the command line. The structure of this file is simple:

    - any line starting with ';' is a comment and is ignored
    - each line has a 'text intensity' pair, separated by TAB
      or the space character(s).
    - the text must be ONE string without whitespace.

Example:

    ;These are data for compound 4711.X
    this_ion    134.021
    that_ion    122.102
    whatanION!  186.999


EXAMPLES

hr -h
    displays the help text, then exits.

hr -v
    displays version information, then exits.

hr
    launches the program in interactive mode. Enter <return> twice to exit.

hr -t 10
    sets mass tolerance to +-10 mmu, then runs the program in interactive
    mode as above.

hr -C 5-15 -S 0-2 -1 0-5
    sets compositional limit for carbon-12 to 5-15 atoms, 0-2 for sulphur,
    0-5 for carbon-13, then runs the program in interactive mode.

hr -c "TBAH" -m 242.28 -t 10
    sets mass tolerance to +-10 mmu, comment text to "TBAH", then runs
    the calculation for mass 242.28 and exits.

hr -c "TBAH" -m 242.28 -t 10 > results.txt
    as above, but redirects output to the file 'results.txt'.

hr -t 10 data.txt > results.txt
    sets mass tolerance to +-10 mmu, reads and processes data from
    'data.txt' and writes the results to 'results.txt'.


LICENSE

This program and its documentation are Copyright (c) 1992-2005 by Joerg Hau.

This program is free software; you can redistribute it and/or modify it under
the terms of version 2 of the GNU General Public License ("GPL") as published
by the Free Software Foundation. See the file LICENSE for details.

If you use this program (or any part of it) in another application, note
that the resulting application becomes also GPL. In other words, GPL is a
"contaminating" license.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License (file
LICENSE) for more details.


OBTAINING THE PROGRAM

The latest version can be obtained via http from the author's website, in
the "Mass Spectrometry" section:

    http://homepage.sunrise.ch/mysunrise/joerg.hau/


FILES

The package you downloaded is a .tar.gz file, which can be handled by most
common "packagers" on most platforms. It contains the following files:

    README      The file you are just reading.
    LICENSE     The license (GPL).

    hr          The executable for Linux platforms (stripped binary).
    hr.exe      The executable for Microsoft OS (stripped binary).
    hr.c        The source code.

    hr.html     Page to use 'hr' with a web server.
    hr.pl       Perl script for using 'hr' with a web server.


To install, just copy the corresponding executable file to any location
you desire.

Invoke it by typing its name. As the program is a command-line utility,
it needs a terminal window. The executable for MS-DOS runs in a "DOS window"
under MS-Windows.

If you want/need to recompile: As this is a single file, I did not provide a
Makefile. This is ANSI C and should compile on any platform. Make sure to
optimize for speed; you may gain a factor of threed! Under Unix/Linux with
gcc installed, use something like

    gcc -O3 -Wall -o hr hr.c

The .exe file included here was generated with gcc under MinGW.


WEB USE

To use this software with a webserver, copy both the executable and the perl
script 'hr.pl' in the webserver's cgi-bin directory.

Copy the HTML page 'hr.html' in the html document tree. This page contains
a simple form that calls 'hr.pl' upon submission. The latter is a Perl script
that reads the parameters, executes hr, and displays the results via HTTP.

If you put this on a webpage, please keep the copyright note intact.



HISTORY

I started the very first "precursor" of the actual program somewhere back
in 1992, as an "interactive" MS-DOS program. After a long period in the
dark, the program was almost completely rewritten in early 2001 - I needed
batch processing capability! At this point, I decided to put it under the
GNU Public License (GPL), and at the same time I changed the version
number(s) to reflect the actual date (in ISO8601 style). 20010418 was the
first public release.

2002-06-27 - added sodium for calculations for Electrospray HiRes data.
           - changed defaults for O to 0-10
2002-10-09 - added 15N (JHa)
2005-02-25 - added -v option, added cgi script; License now "only" GPL v2 (JHa)
2005-02-27 - optimised code in calc loop (JHa)
2005-02-28 - verified and updated atomic masses (JHa)
2005-03-15 - updated README file. No changes to the code (JHa).
2005-06-17 - minor update of Perl wrapper (JHa).


BUGS

If you find any bugs, please contact me by mail (see below). I try to
maintain the program as far as time and work allow.


AUTHOR

    Dr Joerg Hau
    Sur le Jordil
    CH-1148 Cuarnens
    Switzerland

    joerg.hau at dplanet.ch


Thanks you for your interest, and ... have fun!


--eof--
