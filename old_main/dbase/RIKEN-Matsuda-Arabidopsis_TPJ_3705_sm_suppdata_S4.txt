#Standard compound Rt and m/z data
#positive ion mode
#produced by RIKEN Plant Science Center, Metabolome analysis research team, LC-MS branch			#
#The file name should be changed to 'compoundpos.txt'
#
#build 1.02 4.15.2008
#accession	m/z	Rt	Name	Synonyms	CASID	house No.
STD01p00001	176	0.85	L-Citrulline		CAS: 372-75-8	V0063
STD01p00002	435	4.81	Quercetin-3-D-xyloside	Reinutrin;Reynoutrin	CAS: 549-32-6	V0057
STD01p00003	130	1.3	DL-Pipecolic acid	pipecolinic acid;pipecolate;6-carboxypiperidine;pipecolic acid;2-piperidinecarboxylic acid;piperidine-2-carboxylic acid;homoproline;dihydrobaikiane;hexahydropicolinic acid	CAS: 535-75-1;CAS: 1723-00-8(D-);CAS: 3105-95-1(L-);CAS: 4043-87-2;CAS: 69470-51-5	V0042
STD01p00004	178	4.96	D,L-sulforaphane	---	CAS: 4478-93-7	V0041
STD01p00005	319	9.3	zearalenone	---	CAS: 17924-92-4	V0033
STD01p00006	224	7.74	cerulenin	---	CAS: 17397-89-6	V0032
STD01p00007	433	3.64	cyanidin-3-O-rhamnoside chloride	---	CAS: 38533-30-1	V0030
STD01p00008	176	4.92	(-)4-Isothiocyanato-4R-(methylsulfinyl)-1-butene	---	CAS: 592-95-0	V0011
STD01p00009	220	9.05	1-Isothiocyanato-6-(methylsulfonyl)-hexane	---	CAS: ---	V0008
STD01p00010	234	8.32	1-Isothiocyanato-8-(methylsulfinyl)-octane	---	CAS: ---	V0006
STD01p00011	206	6.69	1-Isothiocyanato-6-(methylsulfinyl)-hexane	---	CAS: ---	V0004
STD01p00012	220	7.53	1-Isothiocyanato-7-(methylsulfinyl)-heptane	---	CAS: ---	V0003
STD01p00013	496	2.82	(2R)-2-Hydroxy-2-phenethylglucosinolate	Glucosibarin	CAS: ---	T0124
STD01p00014	482	2.06	p-Hydroxybenzylglucosinolate	Sinalbin	CAS: 19253-840	T0123
STD01p00015	480	3.73	Phenethylglucosinolate	Gluconasturtiin	CAS: 499-30-9	T0121
STD01p00016	466	3	Benzylglucosinolate	Glucotropaeolin	CAS: 5115-71-9	T0120
STD01p00017	492	0.91	4-(Methylsulfinyl)but-3-enylglucosinolate	Glucoraphenin	CAS: 28463-24-3	T0118
STD01p00018	494	1.6	4-(Methylsulfinyl)butylglucosinolate	Glucoraphanin	CAS: 21414-41-5	T0117
STD01p00019	480	1.31	3-(Methylsulfinyl)propylglucosinolate	Glucoiberin	CAS: 554-88-1	T0116
STD01p00020	446	0.91	(2S)-2-Hydroxybut-3-enylglucosinolate	Epiprogoitrin	CAS: 19237-18-4	T0114
STD01p00021	446	0.91	(2R)-2-Hydroxybut-3-enylglucosinolate	Progoitrin	CAS: 585-95-5	T0113
STD01p00022	430	2.26	But-3-enylglucosinolate	Gluconapin	CAS: 19041-09-9	T0112
STD01p00023	271	4.66	pelargonidin chloride	3,4��,5,7-Tetrahydroxyflavylium chloride	CAS: 134-04-3;CAS: 7690-51-9	T0087
STD01p00024	579	3.13	(-)-Epicatechin-(4-beta-8)-(+)-catechin	Procyanidin B1	CAS: 20315-25-7	T0006
STD01p00025	579	3.63	(-)-Epicatechin-(4-beta-8)-(-)-epicatechin	Procyanidin B2	CAS: 29106-49-8	T0005
STD01p00026	291	3.89	(-)-3 3' 4' 5 7-Pentahydroxyflavan	(-)-Epicatechin	CAS: 490-46-0;CAS: 154-23-4;CAS: 7295-85-4;CAS: 2545-08-6	T0004
STD01p00027	291	3.89	(+)-3 3' 4' 5 7-Pentahydroxyflavan	(+)-Epicatechin	CAS: 35323-91-2	T0003
STD01p00028	291	3.46	(+)-3 3' 4' 5 7-Pentahydroxyflavan	(+)-Catechin hydrate	CAS: 88191-48-4;CAS: 154-23-4;CAS: 7295-85-4;(CAS: 225937-10-0[nH2O])	T0002
STD01p00029	611	4.47	quercetin-3-O-rutinoside	Rutin 	CAS:153-18-4;CAS 250249-75-3(trihydrate);CAS 207671-50-9(nH2O);CAS: 207671-50-9;CAS: 115888-40-9;CAS: 1416-01-9;CAS: 146525-66-8;CAS: 158560-09-9;CAS: 164535-43-7;CAS: 18449-50-8;CAS: 48197-72-4;CAS: 56764-99-9	T0001
STD01p00030	414	7.8	Solasodine	---	CAS: 126-17-0	S0347
STD01p00031	398	7.44	Solanidine	---	CAS: 80-78-4	S0346
STD01p00032	417	16.23	Smilagenin	---	CAS: 126-18-1	S0345
STD01p00033	417	16.35	Sarsapogenine	Parigenin	CAS: 126-19-2	S0344
STD01p00034	427	12.21	Friedelin	---	CAS: 559-74-0	S0342
STD01p00035	227	10.1	Methyl Dihydrojasmonate	hedioneR (firmenich);methyl 2-pentyl-3-oxo-1-cyclopentyl acetate;mdj (kao);methyl 2-(3-oxo-2-pentyl cyclopentyl) ethanoate;methyl 3-oxo-2-pentyl cyclopentane acetate;kharismalR (iff);methyl (2-pentyl-3-oxocyclopentyl) acetate;methyl (3-oxo-2-pentyl cyclop	CAS: 24851-98-7	S0340
STD01p00036	223	8.4	Farnesol	---	CAS: 4602-84-0	S0324
STD01p00037	165	5.35	beta-Thujaplicin	Hinokitiol	CAS: 499-44-5	S0323
STD01p00038	223	8.42	alpha-bisabolol	6-methyl-2-(4-methyl-3-cyclohexen-1-yl)-5-hepten-2-ol;dragosantol	CAS: 515-69-5	S0319
STD01p00039	579	4.43	vitexin-2''-O-rhamnoside	---	CAS: 64820-99-1	S0317
STD01p00040	433	4.56	apigenin 8-C-glucoside	Vitexin	CAS: 3681-93-4	S0316
STD01p00041	595	6.14	kaempferol-3-O-(6-p-coumaroyl)-glucoside	Tiliroside	CAS: 20316-62-5	S0315
STD01p00042	509	5.07	syringetin-3-O-glucoside	---	CAS: 40039-49-4	S0314
STD01p00043	509	5.04	syringetin-3-O-galactoside	---	CAS: 55025-56-4	S0313
STD01p00044	465	5.2	quercetin-4'-glucoside	Spiraeoside;Spiraein	CAS: 20229-56-5	S0312
STD01p00045	447	6.75	Sissotrin	Biochanin a-7-Glucoside;astroside;(biochanin a 7-o-glucoside)	CAS: 5928-26-7	S0311
STD01p00046	595	4.03	apigenin-6-C-glucoside -7-O-glucoside	Saponarin;Isovitexin-7-O-glucoside;Saponaretin-7-O-glucoside	CAS: 20310-89-8	S0310
STD01p00047	287	8.82	4',5-dihydroxy-7-methoxyflavone	Sakuranetin;Naringenin-7-methylether	CAS: 2957-21-3 	S0309
STD01p00048	595	6.39	isosakuranetin-7-O-neohesperidoside	Poncirin	CAS: 14941-08-3	S0308
STD01p00049	437	5.52	phloretin-2'-O-glucoside	Phloridzin	CAS: 60-81-1;CAS: 112318-65-7;CAS: 16055-86-0;CAS: 52276-56-9	S0307
STD01p00050	449	4.25	luteolin-8-C-glucoside	Orientin;Lutexin;(luteolin 8-c-beta-d-glucopyranoside)	CAS: 28608-75-5	S0306
STD01p00051	431	5.86	formononetin-7-O-glucoside	Ononin	CAS: 486-62-4	S0305
STD01p00052	613	5.95	dihydrohesperetin-7-O-neohesperidoside	Neohesperidin dihydrochalcone	CAS: 20702-77-6	S0304
STD01p00053	611	5.37	hesperetin-7-O-neohesperidoside	Neohesperidin	CAS: 13241-33-3	S0303
STD01p00054	597	4.67	eriodictyol-7-O-neohesperidoside	Neoeriocitrin	CAS: 13241-32-2;CAS: 36790-48-4;CAS: 39280-04-1	S0302
STD01p00055	609	5.24	diosmetin-7-O-neohesperidoside	Neodiosmin	CAS: 38665-01-9	S0301
STD01p00056	449	4.61	maritimetin-6-O-glucoside	Maritimein;3',4'6,7-Tetrahydroxy-6-O-glucosylaurone	CAS: 490-54-0	S0299
STD01p00057	451	4.73	okanin-4'-O-glucoside	Marein;2',3,3',4,4'-Pentahydroxy-4'-glucosylchalcone	CAS: 535-96-6	S0298
STD01p00058	449	5.2	luteolin-4'-O-glucoside	---	CAS: 6920-38-3	S0297
STD01p00059	593	6.11	acacetin-7-O-rutinoside	Linarin;Acaciin	CAS: 480-36-4	S0295
STD01p00060	287	8.86	Isosakuranetin	4'-Methylnaringenin;'4'-Methoxy-5,7-dihydroxyflavonone	CAS: 480-43-3	S0292
STD01p00061	611	5.24	hesperetin-7-O-rutinoside	Hesperidin	CAS: 520-26-3;CAS: 106904-63-6;CAS: 15512-51-3;CAS: 16643-24-6;CAS: 17654-22-7;CAS: 28283-75-2;CAS: 30927-97-0;CAS: 32737-61-4	S0291
STD01p00062	481	5.43	gossypetin-8-glucoside	Gossypin;3,5,7,3',4'-Pentahydroxy-8-O-glucosylflavone	CAS: 652-78-8	S0290
STD01p00063	593	6.2	acacetin-7-O-neohesperidoside	Fortunellin	CAS: 20633-93-6	S0288
STD01p00064	451	3.94	isookanin-7-glucoside	Flavanomarein	CAS: 577-38-8	S0287
STD01p00065	609	5.13	Diosmin	Barosmin;Diosmetin-7-Rutinoside	CAS: 520-27-4	S0286
STD01p00066	595	6.4	2', 6'-Dihydroxy-4-Methoxychalcone-4'-O-Neohesperidoside	---	CAS: ---	S0285
STD01p00067	421	5.65	4-deoxyphloridzin	---	CAS: ---	S0284
STD01p00068	595	4.76	datiscetin-3-O-rutinoside	Datiscin	CAS: 16310-92-2	S0283
STD01p00069	285	8.79	acacetin	Apigenin-4'-methylether(5,7-Dihydroxy-4'-methoxyflavone)	CAS: 480-44-4	S0282
STD01p00070	463	3.66	peonidin-3-O-glucoside chloride	oxycoccicyanin;(peonidin 3-o-beta-d-glucopyranoside)	CAS: 6906-39-4	S0281
STD01p00071	493	3.69	malvidin-3-O-glucoside chloride	Oenin;Cyclamin;(malvidin 3-o-beta-d-glucopyranoside)	CAS: 7228-78-6	S0279
STD01p00072	655	3.12	malvidin-3, 5-di-O-glucoside chloride	Malvin chloride 	CAS: 16727-30-3	S0277
STD01p00073	493	3.61	malvidin-3-galactoside chloride	Primulin Chloride;(malvidin 3-o-beta-d-galactopyranoside)	CAS: 30113-37-2	S0276
STD01p00074	449	3.28	cyanidin-3-glucoside chloride	Kuromanin chloride	CAS: 7084-24-4	S0275
STD01p00075	449	3.18	cyanidin-3-O-galactoside chloride	Ideain chloride	CAS: 27661-36-5	S0274
STD01p00076	611	2.74	cyanidin-3, 5-di-O-glucoside chloride	Cyanin chloride	CAS: 2611-67-8	S0272
STD01p00077	412	7.74	Phosphocholine, 1-Decanoyl-2-Hydroxy-sn-Glycero-3- 	---	CAS: 22248-63-1	S0251
STD01p00078	440	9.15	Phosphocholine, 1-Lauroyl-2-Hydroxy-sn-Glycero-3- 	---	CAS: 20559-18-6	S0250
STD01p00079	150	0.81	Triethanolamine	---	CAS: 102-71-6	S0230
STD01p00080	209	5.42	trans-3,5-Dimethoxy-4-hydroxycinnamaldehyde	Sinapinaldehyde	CAS: 4206-58-0	S0221
STD01p00081	229	5.83	Resveratrol	3,4,5-Trihydroxystilbene	CAS: 501-36-0	S0219
STD01p00082	303	7.25	Hesperetin	3',5,7-Trihydroxy-4'-	CAS: 41001-90-5 ;CAS: 69097-99-0 ;CAS: 520-33-2	S0200
STD01p00083	183	4.72	4-Hydroxy-3,5-dimethoxybenzaldehyde	Syringaldehyde	CAS: 134-96-3	S0150
STD01p00084	382	2.64	Zeatin-9-glucoside	trans-Zeatinglucoside	CAS: 51255-96-0	S0149
STD01p00085	382	4.46	Zeatin-9-glucoside	trans-Zeatinglucoside	CAS: 51255-96-0	S0149
STD01p00086	336	4.77	Isopentenyladenosine	  	CAS: 7724-76-7	S0136
STD01p00087	153	4.58	Vanillin	---	CAS: 121-33-5	S0132
STD01p00088	220	2.65	trans-Zeatin	---	CAS: 1637-39-4	S0128
STD01p00089	162	0.84	L-Carnitine	Vitamin BT	CAS: 541-15-1	S0114
STD01p00090	324	0.91	CMP	Cytidine-5�f-monophosphate monohydrate;Cytidylic acid	CAS: 63-37-6	S0105
STD01p00091	489	0.86	CDP-choline	Cytidine 5'-diphosphocholine	CAS: 987-78-0	S0102
STD01p00092	537	17	alpha-Carotene	---	CAS: 7488-99-5	S0100
STD01p00093	225	9.32	(-)-Jasmonic acid methyl ester;Methyljasmonate	Methyljasmonate	CAS: 1211-29-6	S0094
STD01p00094	220	2.78	cis-Zeatin	---	CAS: 32771-64-5	S0093
STD01p00095	305	4.89	(+-)-Taxifolin	(+)-Dihydroquercetin	CAS: 480-18-2	S0088
STD01p00096	328	4.37	Scoulerin	---	CAS: 6451-73-6	S0085
STD01p00097	389	4.44	Secologanin	---	CAS: 19351-63-4	S0084
STD01p00098	380	2.07	S-Lactoylglutathione	---	CAS: 54398-03-7	S0074
STD01p00099	258	0.84	L-alpha-Glycerophosphorylcholine 1:1 cadmium chloride adduct	---	CAS: 64681-08-9	S0069
STD01p00100	269	7.87	Formononetin	Biochanin B ; Neochanin ; 7-Hydroxy-4'-Methoxyisoflavone	CAS: 485-72-3	S0068
STD01p00101	222	2.73	DL-Dihydrozeatin	---	CAS: 23599-75-9	S0065
STD01p00102	298	2.98	5'-Deoxy-5'-Methylthioadenosine	---	CAS: 2457-80-9	S0063
STD01p00103	401	12.02	Campesterol	---	CAS: 474-62-4	S0055
STD01p00104	199	3.96	Syringate	Syringic Acid	CAS: 530-57-4	S0042
STD01p00105	149	5.21	(S)-(+)-Citramailc acid1	---	CAS: 6236-09-5;CAS: 102601-31-0(+/-)	S0041
STD01p00106	144	2.27	4-Methyl-5-thiazoleethanol	---	CAS: 137-00-8	S0039
STD01p00107	122	2.54	N,N-Dimethylaniline	---	CAS: 121-69-7	S0037
STD01p00108	285	2.22	Xanthosine	---	CAS: 5968-90-1;CAS: 146-80-5	S0032
STD01p00109	347	4.74	Gibberellic acid	---	CAS: 77-06-5	S0031
STD01p00110	140	2.06	6-Hydroxynicotinate	6-Hydroxynicotinic Acid	CAS: 5006-66-6	S0024
STD01p00111	220	2.65	Sodium D-pantothenate	D-Pantothenic Acid Sodium Salt	CAS: 867-81-2;CAS: 79-83-4	S0022
STD01p00112	355	3.42	Chlorogenic acid Hemihydrate	---	CAS: 327-97-9	S0021
STD01p00113	140	0.91	Tropinone	---	CAS: 532-24-1	S0020
STD01p00114	193	0.9	�i1R,3R,4R,5R)-(-)-Quinic acid	D-(-)-Quinic acid	CAS: 77-95-2	S0011
STD01p00115	206	6.14	DL-alpha-Lipoamide	Tioctamide	CAS: 940-69-2	S0009
STD01p00116	248	1.51	Pyridoxal 5-phosphate monohydrate 	---	CAS: 41468-25-1;CAS: 54-47-7	S0002
STD01p00117	311	3.74	Sinapine	o-Sinapoylcholine;Sinapine bisulphate;4-Hydroxy-3,5-dimethoxycinnamate choline 	CAS: 18696-26-9?---	pos_R29
STD01p00118	311	3.87	Sinapine	o-Sinapoylcholine;Sinapine bisulphate;4-Hydroxy-3,5-dimethoxycinnamate choline 	CAS: 18696-26-9?---	pos_R29
STD01p00119	387	3.76	1-O-b-D-glucopyranosyl sinapate	1-O-Sinapoyl-beta-D-glucose;1-O-Sinapoyl beta-D-glucoside	CAS: ---	pos_R28_2
STD01p00120	387	3.9	1-O-b-D-glucopyranosyl sinapate	1-O-Sinapoyl-beta-D-glucose;1-O-Sinapoyl beta-D-glucoside	CAS: ---	pos_R28_2
STD01p00121	341	4.82	Sinapoyl malate	Sinapoyl-(S)-malate;2-O-sinapoylmalate 	CAS: ---	pos_R26
STD01p00122	341	4.9	Sinapoyl malate	Sinapoyl-(S)-malate;2-O-sinapoylmalate 	CAS: ---	pos_R26
STD01p00123	595	4.21	Kaempferol-3-O-b-glucopyranosyl-7-O-a-rhamnopyranoside	Kaempferol 3-glucoside-7-rhamnoside	CAS: 2392-95-2	pos_R23_3
STD01p00124	611	3.92	Quercetin-3-O-b-glucopyranosyl-7-O-a-rhamnopyranoside	SPECIES:;quercetin 3-glucoside-7-rhamnoside	CAS: 18016-58-5 	pos_R22_3
STD01p00125	757	3.5	Quercetin-3-O-a-L-rhamnopyranosyl(1,2)-b-D-glucopyranoside-7-O-a-L-rhamnopyranoside	quercetin 3-neohesperidoside-7-rhamnoside ; 3-[[2-O-(6-Deoxy-alpha-L-mannopyranosyl)-beta-D-glucopyranosyl]oxy]-7-[(6-deoxy-alpha-L-mannopyranosyl)oxy]-2-(3,4-dihydroxyphenyl)-5-hydroxy-4H-1-benzopyran-4-one	CAS: 161993-01-7	pos_R21_3
STD01p00126	741	3.69	Kaempferol-3-O-a-L-rhamnopyranosyl(1,2)-b-D-glucopyranoside-7-O-a-L-rhamnopyranoside	kaempferol 3-neohesperidoside-7-rhamnoside	CAS: 162062-89-7	pos_R20_3
STD01p00127	268	0.93	Adenosine	9-beta-D-Ribofuranosyladenine;Adenine riboside;Sandesin ;Boniton;Myocol ;Nucleocardyl  	CAS: 58-61-7	pos_R18
STD01p00128	268	1.92	Adenosine	9-beta-D-Ribofuranosyladenine;Adenine riboside;Sandesin ;Boniton;Myocol ;Nucleocardyl  	CAS: 58-61-7	pos_R18
STD01p00129	449	5.52	Quercetin-7-O-rhamnoside	Vincetoxicoside B	CAS: 22007-72-3	pos_R16
STD01p00130	595	4.25	Quercetin-3,7-O-a-L-dirhamnopyranoside		CAS: 28638-13-3	pos_R15_3
STD01p00131	449	5.03	Quercetin-3-O-a-L-rhamnopyranoside	Quercitrin;thujin                                        Quercitrin                                      ;thujin	CAS: 522-12-3;CAS: 117-39-5;CAS: 6151-25-3(2H2O)	pos_R14
STD01p00132	579	4.56	Kaempferol 3,7-O-dirhamnopyranoside	Kaempferitrin	CAS: 482-38-2	pos_R09_4
STD01p00133	162	5.23	Indole-3-carboxylic acid	1H-Indole-3-carboxylic acid;3-Indoleformic Acid	CAS: 771-50-6	pos_R08
STD01p00134	176	7.14	1-Methoxy-3-carbaldehyde	1-methoxyindole-3-carbaldehyde;3-Formyl-1-methoxyindole;1-Methoxy-3-formylindole	CAS: ---	pos_R07
STD01p00135	146	5.34	Indole-3-aldehyde	3-Formylindole;Indole-3-carbaldehyde;1H-indole-3-carbaldehyde;beta-Indolylaldehyde;Indole-3-carboxaldehyde 	CAS: 487-89-8	pos_R06
STD01p00136	225	4.74	Sinapic acid	3 5-dimethoxy-4-hydroxycinnamic acid	CAS: 530-59-6	pos_R05
STD01p00137	225	5.01	Sinapic acid	3 5-dimethoxy-4-hydroxycinnamic acid	CAS: 530-59-6	pos_R05
STD01p00138	183	4.68	Syringaldehyde	4-Hydroxy-3,5-dimethoxybenzaldehyde;3,5-Dimethoxy-4-hydroxybenzene carbonal	CAS: 134-96-3	pos_R04
STD01p00139	225	5.45	5,6-Epoxy-3-hydroxy-9-apo-beta-caroten-9-one	Apocarotenoid	CAS: ---	pos_R03
STD01p00140	1063	2.19	Heptasaccharide ,Glc4Xyl3		CAS: 121591-98-8	N0124
STD01p00141	1387	2.19	Nonasaccharide,Glc4Xyl3Gal2		CAS: 129865-06-1	N0123
STD01p00142	118	0.85	Betaine		CAS: 107-43-7	N0117
STD01p00143	184	2.11	4-Pyridoxate	4-Pyridoxic acid;3-Hydroxy-5-(hydroxymethyl)-2-methyl-4-pyridinecarboxylic acid;2-Methyl-3-hydroxy-4-carboxy-5-hydroxymethylpyridine  	CAS: 82-82-6	N0099
STD01p00144	167	2.27	3-Methylxanthine	---	CAS: 1076-22-8	N0089
STD01p00145	181	2.98	1,7-Dimethylxanthine	---	CAS: 611-59-6	N0088
STD01p00146	197	2.55	1,3-Dimethylurate	1,3-Dimethyluric acid  	CAS: 944-73-0	N0087
STD01p00147	137	0.83	1-Methylnicotinamide chloride	---	CAS: 1005-24-9	N0085
STD01p00148	138	0.89	Trigonelline hydrochloride	---	CAS: 6138-41-6;CAS: 535-83-1	N0084
STD01p00149	152	2.84	Acetaminophen	---	CAS: 103-90-2	N0081
STD01p00150	190	3.33	Kynurenate	Kynurenic acid ;4-Hydroxyquinoline-2-carboxylic acid	CAS: 492-27-3	N0078
STD01p00151	153	1.83	Oxypurinol	---	CAS: 2465-59-0	N0068
STD01p00152	123	1.39	Niacinamide	Vitamin B3	CAS: 98-92-0	N0067
STD01p00153	220	2.66	D-Pantothenic acid hemicalcium salt	Vitamin B5	CAS: 137-08-6;CAS: 79-83-4	N0064
STD01p00154	377	3.86	(-)-Riboflavin	Vitamin B2	CAS: 83-88-5	N0062
STD01p00155	115	1.3	5,6-Dihydrouracil	---	CAS: 504-07-4	N0051
STD01p00156	181	2.99	Theophylline,anhydrous	1,3-Dimethylxanthine	CAS: 58-55-9	N0050
STD01p00157	227	0.78	L-Carnosine	---	CAS: 305-84-0	N0036
STD01p00158	131	0.91	N-acetyl putrescine hydrochloride	---	CAS: 18233-70-0	H0096
STD01p00159	168	1.34	pyridoxal hydrochrolide	---	CAS: 65-22-5	H0089
STD01p00160	139	1.3	urocanic acid	4-Imidazoleacrylic acid 	CAS: 104-98-3	H0070
STD01p00161	105	3.17	3-cyanopyridine	3-Pyridinecarbonitrile 	CAS: 100-54-9	H0066
STD01p00162	177	3.81	(S)-(+)-2-(anilinomethyl)pyrrolidine	---	CAS: 64030-44-0	H0065
STD01p00163	294	9.46	nordihydrocapsaicin	---	CAS: 28789-35-7	H0051
STD01p00164	308	10.27	N-[(4-hydroxy-3-methoxyphenyl)methyl]-8-methyl-nonanamide	Dihydrocapsaicin	CAS: 19408-84-5	H0050
STD01p00165	306	9.58	8-methyl-N-vanillyl-6-nonenamide	Capsaicin	CAS: 404-86-4	H0049
STD01p00166	823	7.94	glycyrrhizic Acid	Glycyrrhizin	CAS: 1405-86-3	H0048
STD01p00167	417	4.16	4'-hydroxyisoflavone-7-glucoside	Daidzin;Cannabiscetin	CAS: 552-66-9	H0046
STD01p00168	449	5.09	quercetin-3-O-rhamnoside		CAS: 522-12-3;CAS: 117-39-5;CAS: 6151-25-3(2H2O)	H0043
STD01p00169	507	5.15	quercetin-3-O-beta-D-glucopyranosyl-6''-acetate	quercetin 3-(6''-acetylglucoside)	CAS: 54542-51-7	H0042
STD01p00170	465	4.66	quercetin-3-O-beta-glucopyranoside	Hirsutrin;Isoquercetin;(quercetin 3-beta-d-glucoside)	CAS: 482-35-9;CAS: 21637-25-2	H0041
STD01p00171	465	4.61	quercetin-3-beta-O-galactoside	hyperin;Hyperoside	CAS: 482-36-0	H0040
STD01p00172	597	4.27	quercetin-3-O-arabinoglucoside	Peltatoside	CAS: 23284-18-6	H0039
STD01p00173	417	3.7	daidzein-8-C-glucoside	Puerarin;kakonein;4' 7-dihydroxy-8-C-glucosylisoflavone	CAS: 3681-99-0	H0038
STD01p00174	433	3.55	pelargonin-3-O-glucoside chloride	Callistephin Chloride;(pelargonidin 3-o-beta-d-glucopyranoside)	CAS: 18466-51-8	H0037
STD01p00175	435	5.25	naringenin-7-O-glucoside	Prunin	CAS: 529-55-5	H0036
STD01p00176	465	4.58	myricetin-3-O-rhamnoside	Myricitrin	CAS: 17912-87-7	H0035
STD01p00177	449	4.71	luteolin-7-O-glucoside	Cynaroside;Glucoluteolin	CAS: 5373-11-5;CAS: 26811-41-6	H0034
STD01p00178	449	4.1	luteolin-6-C-glucoside	Homoorietin;Isoorientin;(luteolin 6-c-beta-d-glucopyranoside)	CAS: 4261-42-1	H0033
STD01p00179	595	5.04	kaempferol-7-O-neohesperidoside	---	CAS: 13353-03-6;CAS: 17353-03-6	H0032
STD01p00180	595	4.83	kaempferol-3-O-rutinoside	Nicotiflorin	CAS: 17650-84-9	H0031
STD01p00181	449	5.03	kaempferol-3-O-glucoside	Astragalin	CAS: 480-10-4	H0030
STD01p00182	741	4.1	kaempferol-3-O-robinoside-7-O-rhamnoside	Robinin	CAS: 301-19-9;CAS: 81992-85-0	H0029
STD01p00183	625	4.89	isorhamnetin-3-O-rutinoside	Narcissin	CAS: 604-80-8;CAS: 56384-79-3;CAS: 31228-20-3	H0028
STD01p00184	479	5.1	isorhamnetin-3-O-glucoside	---	CAS: 5041-82-7	H0027
STD01p00185	451	4.73	eriodictyol-7-O-glucoside	Pyracanthoside	CAS: 38965-51-4	H0026
STD01p00186	595	3.33	cyanidin-3-O-rutinoside chloride			H0024
STD01p00187	447	5.81	baicalein-7-O-glucuronide			H0023
STD01p00188	579	5.07	apigenin-7-O-neohesperidoside			H0022
STD01p00189	433	5.19	apigenin-7-O-glucoside			H0021
STD01p00190	317	8.05	3 3' 4' 5-tetrahydroxy-7-methoxyflavone	Rhamnetin	CAS: 90-19-7	H0018
STD01p00191	303	6.3	3 3' 4' 5 7-pentahydroxyflavone	Quercetin	CAS: 117-39-5(net);CAS:6151-25-3(paper) ;CAS: 7255-55-2;CAS: 73123-10-1;CAS: 74893-81-5	H0017
STD01p00192	595	2.99	pelargonidin-3,5-di-O-glucoside	Pelargonin chloride;(pelargonidin 3,5-di-beta-d-glucoside)	CAS: 177334-58-6	H0016
STD01p00193	273	6.99	4' 5 7-trihydroxyflavanone	Naringenin	CAS: 67604-48-2;CAS: 480-41-1	H0015
STD01p00194	319	5.44	3 3' 4' 5 5' 7-hexahydroxyflavone	Myricetin;Cannabiscetin	CAS: 529-44-2	H0014
STD01p00195	332	3.83	3 4' 5 7-tetrahydroxy-3' 5'-dimethoxyflavylium chloride	Malvidin Chloride	CAS: 643-84-5	H0013
STD01p00196	287	6.27	3' 4' 5 7-tetrahydroxyflavone	Luteolin	CAS: 491-70-3	H0012
STD01p00197	301	9.01	3 5 7-trihydroxy-4'-methoxyflavone	Kaempferide	CAS: 491-54-3	H0011
STD01p00198	287	7.09	3 4' 5 7-tetrahydroxyflavone	Kaempferol;nimbecetin;pelargidenolon;rhamnolutein	CAS: 520-18-3	H0010
STD01p00199	317	7.22	3 4' 5 7-tetrahydroxy-3'-methoxy flavone	Isorhamnetin	CAS: 480-19-3	H0009
STD01p00200	271	6.99	4' 5 7-trihydroxyisoflavone	Genistein	CAS: 446-72-0	H0008
STD01p00201	225	8.42	2 3-dihydroflavone	Flavanone	CAS: 487-26-3	H0007
STD01p00202	289	6.26	3' 4' 5 7-tetrahydroxyflavanone	Eriodictyol	CAS: 4049-38-1/CAS: 552-58-9;CAS: 552-016-4	H0006
STD01p00203	255	6.03	4' 7-dihydroxyisoflavone	Daidzein	CAS: 486-66-8	H0005
STD01p00204	271	6.96	4' 5 7-trihydroxyflavone	Apigenin	CAS: 520-36-5	H0001
STD01p00205	335	0.91	beta-Nicotinamide mononucleotide	---	CAS: 1094-61-7	387
STD01p00206	664	0.91	beta-Nicotinamide adenine dinucleotide oxidized form	beta-NMN	CAS: 53-84-9	386
STD01p00207	153	1.68	Xanthine	---	CAS: 69-89-6	373
STD01p00208	245	1.83	Uridine	---	CAS: 58-96-8	365
STD01p00209	138	1.97	Tyramine	4-Hydroxy-.beta.-phenylethylamine	CAS: 51-67-2	364
STD01p00210	352	3.2	trans-Zeatin-riboside	---	CAS: 6025-53-2	363
STD01p00211	127	2.04	Thymine	5-Methyluracil	CAS: 65-71-4	361
STD01p00212	243	2.41	Thymidine	---	CAS: 50-89-5	355
STD01p00213	202	3.47	Thiabendazole	---	CAS: 148-79-8	349
STD01p00214	126	0.83	Taurine	2-Aminoethanesulfonic Acid	CAS: 107-35-7	347
STD01p00215	170	1.75	Pyridoxine	Vitamin B6	CAS: 65-23-6;CAS: 58-56-0(Hydrochloride)	332
STD01p00216	138	0.92	Pyridine-2-aldoxime methochloride	---	CAS: 51-15-0	331
STD01p00217	187	7.12	Psoralen	---	CAS: 66-97-7	330
STD01p00218	303	0.82	PIPES	1,4-Piperazinediethanesulfonic acid;Piperazine-1,4-bis(2-ethanesulfonic acid)	CAS: 5625-37-6	324
STD01p00219	518	6.54	Piperacillin sodium salt	---	CAS: 59703-84-3	322
STD01p00220	157	0.91	Orotic acid ,Anhydrous	6-Carboxyuracil;Vitamin B13	CAS: 65-86-1	317
STD01p00221	336	0.91	Nicotinic acid nucleotide mono	---	CAS: 321-02-8	309
STD01p00222	210	4.13	Minoxidil	6-(1-Piperidinyl)-2,4-pyrimidinediamine 3-oxide	CAS: 38304-91-5	301
STD01p00223	168	4.36	Mecamylamine hydrochloride	---	CAS: 826-39-1	296
STD01p00224	124	0.91	Isonicotinic acid	4-Pyridinecarboxylic Acid	CAS: 55-22-1	262
STD01p00225	128	0.85	Isoguvacine hydrochloride	---	CAS: 64603-90-3	261
STD01p00226	269	2.05	Inosine	---	CAS: 58-63-9	256
STD01p00227	137	1.45	Hypoxanthine	---	CAS: 68-94-0	252
STD01p00228	110	0.83	Hypotaurine	---	CAS: 300-84-5	251
STD01p00229	284	2.04	Guanosine	---	CAS: 118-00-3	241
STD01p00230	152	1.29	Guanine	---	CAS: 73-40-5	240
STD01p00231	823	7.93	Glycyrrhizic acid ammonium salt	---	CAS: 53956-04-0	238
STD01p00232	613	1.88	Glutathione (oxidized form)	---	CAS: 27025-41-8	235
STD01p00233	333	8.04	Gibberellin A4	---	CAS: 468-44-0	233
STD01p00234	457	3.53	Flavin mononucleotide	Riboflavin 5��-monophosphate sodium salt hydrate 	CAS: 130-40-5;CAS: 6184-17-4	229
STD01p00235	276	3.83	Eserine	Physostigmine;Antilirium;Physostol;Esromiotin;Cogmine	CAS: 57-47-6	226
STD01p00236	106	0.8	Diethanolamine	2,2'-Iminodiethanol;2,2'-Dihydroxydiethylamine	CAS: 111-42-2	210
STD01p00237	162	0.84	D-Carnitine hydrochloride salt	D-Vitamin BT ;beta-Hydroxy-gamma-trimethylaminobutyric Acid Hydrochloride	CAS: 10017-44-4;CAS: 541-14-0(544361);461-05-2	205
STD01p00238	112	0.85	Cytosine	4-Amino-2-hydroxypyrimidine;6-Aminouracil	CAS: 71-30-7	193
STD01p00239	244	0.91	Cytidine,cell culture tested	Cytosine beta-D-riboside;1-beta-D-Ribofuranoside cytosine	CAS: 65-46-3	188
STD01p00240	114	0.85	Creatinine,anhydrous	2-Amino-1-methylimidazolidin-4-one;1-Methylglycocyamidine	CAS: 60-27-5	185
STD01p00241	132	0.89	Creatine,anhydrous	N-Amidinosarcosine;(a-Methylguanido)acetic acid	CAS: 57-00-1	184
STD01p00242	152	3.98	Amantadine hydrochloride	1-Adamantylamine hydrochloride;Tricyclodecane Hydrochloride	CAS: 665-66-7;CAS: 211-560-2	170
STD01p00243	228	12.94	2'-Deoxycytidine	Cytosine deoxyriboside;dCYD	CAS: 951-77-9	144
STD01p00244	252	2	2'-Deoxyadenosine monohydrate	Adenine deoxyriboside	CAS: 16373-93-6	140
STD01p00245	163	0.92	(-)-Nicotine	beta-Pyridyl-alpha-N-methylpyrrolidine	CAS: 54-11-5	136
STD01p00246	164	1.85	Pterine	2-Amino-4-hydroxypteridine	CAS: 2236-60-4	132
STD01p00247	427	5.01	Leupeptin hemisulfate salt	Acetyl-Leu-Leu-Arg-al;N-Acetyl-L-leucyl-L-leucyl-L-argininal hemisulfate salt	CAS: 103476-89-7	128
STD01p00248	215	4.83	Harmaline hydrochloride dihydrate	1-Methyl-7-methoxy-3,4-dihydro-beta-carboline hydrochloride	CAS: 6027-98-1	125
STD01p00249	428	15.61	Adenosine 5'-phosphosulfate	APS	CAS: 102029-95-8	111
STD01p00250	150	1.3	3-Methyladenine	6-Amino-3-methylpurine	CAS: 5142-23-4	106
STD01p00251	133	7.32	trans-Cinnamaldehyde	trans-3-Phenyl-2-propenal;trans-Phenylacrolein	CAS: 14371-10-9	104
STD01p00252	211	4.52	Sinapyl alcohol	4-Hydroxy-3,5-dimethoxycinnamyl alcohol	CAS: 537-33-7	100
STD01p00253	169	0.8	Pyridoxamine dihydrochloride  	---	CAS: 524-36-7	99
STD01p00254	267	9.09	Pentachlorophenol	---	CAS: 87-86-5	96
STD01p00255	146	5.39	Indole-3-carboxyaldehyde	---	CAS: 487-89-8	91
STD01p00256	102	3.62	Hexylamine	1-Aminohexane	CAS: 111-26-2	89
STD01p00257	121	9.08	Glycolaldehyde dimer,mixture of stereoisomers	---	CAS: 23147-58-2	88
STD01p00258	157	7.05	3-Indolylacetonitrile	Indole-3-acetonitrile	CAS: 771-51-7	81
STD01p00259	124	1.29	Nicotinic Acid	Pyridine-3-carbonic acid;Vitamin B3	CAS: 59-67-6	76
STD01p00260	177	5.55	7-Hydroxy-4-methylcoumarin	4-Methylumbelliferone	CAS: 90-33-5	70
STD01p00261	161	3.55	Tryptamine	3-(2-Aminoethyl)indole Hydrochloride	CAS: 61-54-1	69
STD01p00262	113	1.29	Uracil	2,4-Dihydroxypyrimidine	CAS: 66-22-8	61
STD01p00263	195	3.54	Caffeine,Anhydrous	1,3,7-Trimethylxanthine;1,3,7-Trimethyl-2,6-dioxopurine	CAS: 58-08-2	45
STD01p00264	177	0.86	Allantoic acid	Diureidoacetic acid	CAS: 99-16-1	41
STD01p00265	268	1.97	Adenosine	9-beta-D-Ribofuranosyladenine;Adenine riboside;Sandesin ;Boniton;Myocol ;Nucleocardyl  	CAS: 58-61-7	40
STD01p00266	136	1.3	Adenine	6-Aminopurine ; Vitamin B4	CAS: 73-24-5	39
STD01p00267	204	4.15	6-(gamma,gamma-Dimethylallylamino)purine	2iP ; N6-(2-Isopentenyl)adenine ; Triacanthine	CAS: 2365-40-4	37
STD01p00268	126	0.91	5-Methylcytosine hydrochloride	4-Amino-2-hydroxy-5-methylpyrimidine Hydrochloride	CAS: 58366-64-6	35
STD01p00269	177	2.26	5-Hydoxytryptamine hydrochloride	Serotonin hydrochloride ;3-(2-Aminoethyl)-5-hydroxyindole hydrochloride	CAS: 153-98-0	34
STD01p00270	110	17.94	4-Aminophenol hydrochloride	4-Hydroxyaniline hydrochloride	CAS: 51-78-5	30
STD01p00271	174	4.13	1-Methyl-4-phenyl-1,2,3,6-tetrahydropyridine	MPTP	CAS: 28289-54-5	27
STD01p00272	130	0.91	1,1-Dimethylbiguanide hydrochloride	Metformin hydrochloride	CAS: 1115-70-4	22
STD01p00273	168	1.92	(R)-(-)-Phenylephrine hydrochloride	L-(3-Hydroxyphenyl)-N-methylethanolamine hydrochloride	CAS: 61-76-7	20
STD01p00274	451	14.82	2-Methyl-3-phytyl-1,4-naphthoquinone	Vitamin K1	CAS: 84-80-0	19
STD01p00275	214	3.74	(+-)-Baclofen	beta-(Aminomethyl)-p-chlorohydrocinnamic acid ; gamma-amino-beta-(p-chlorophenyl)butyric acid ; (+-)-beta-(Aminomethyl)-4-chlorobenzenepropanoic acid	CAS: 1134-47-0	17
STD01p00276	336	4.76	N-6-(delta-2-Isopentenyl)adenosinehemihydrate	Riboprine;Isopentenyladenosine;Dimethylallyladenosine;N-Isopentenyladenosine;2iPA	CAS: 33156-15-9;CAS: 7724-76-7	13
STD01p00277	179	5.46	4-Hydroxy-3-methoxycinnamaldehyde	Coniferyl aldehyde ; Coniferaldehyde ; Ferulaldehyde ; 	CAS: 458-36-6	6
STD01p00278	505	3.38	indole-3-ylmethyl-glucosinolate	I3M	CAS: 4356-52-9 	X
STD01p00279	535	3.89	4-methoxyindole-3-ylmethyl-glucosinolate	4MI3M	CAS: 83327-21-3	X
STD01p00280	535	4.49	1-methoxyindole-3-ylmethyl-glucosinolate	1MI3M	CAS: 5187-84-8	X
