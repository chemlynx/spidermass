import pytest
import spidermassfunc


def test_loading_mass_data():
    return spidermassfunc.load_mass_data()


masses = test_loading_mass_data()
assert len(masses) == 6
assert masses[0] == 0.000549
assert masses[1] == 1.0078250321
assert masses[2] == 22.9897692809
assert masses[3] == 38.96370668
assert masses[4] == 14.0030740048
assert masses[5] == 18.0343741332


@pytest.fixture
def ionSpecies_data():
    MMW_e, MMW_H, MMW_Na, MMW_K, MMW_N, MMW_NH4 = spidermassfunc.load_mass_data()

    ionSpecies = [
        {
            'label': 'hydrogencheck',
            'status': True,
            'species': '[M+H]+',
            'modifiers': [MMW_e, MMW_H],
        },
        {
            'label': 'sodiumcheck',
            'status': True,
            'species': '[M+Na]+',
            'modifiers': [MMW_e, MMW_Na],
        },
        {
            'label': 'potassiumcheck',
            'status': True,
            'species': '[M+k]+',
            'modifiers': [MMW_e, MMW_K],
        },
        {
            'label': 'ammoniumcheck',
            'status': False,
            'species': '[M+NH4]',
            'modifiers': [MMW_e, MMW_NH4],
        },
        {
            'label': 'zerocheck',
            'status': True,
            'species': '+/- 0',
            'modifiers': [0, 0],
        },
        {
            'label': 'minusHcheck',
            'status': True,
            'species': '[M-H]-',
            'modifiers': [(-1 * MMW_e), (-1 * MMW_H)],
        },
    ]
    print(ionSpecies)
    return ionSpecies


def test_set_ion_species(ionSpecies_data):
    new_ion_species = set_ion_species(ionSpecies_data, 200.001, 0.001)
    assert len(new_ion_species) == 5
    assert new_ion_species[0]['corrected_parent_mass'] == 198.99372396790002


def test_IDF_checker():
    res = IDF_checker('C2H6', '[M+H]+')

    assert res == 0.8474287796091349
