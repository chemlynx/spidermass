import time

import chemspiderREST
import pytest


def test_isValidKey():
    return chemspiderREST.TOKEN


assert test_isValidKey() != '0'


def test_isValidKey2():
    return type(chemspiderREST.TOKEN)


assert test_isValidKey2() is str


def test_methaneByMass():
    results = chemspiderREST.IntrinsicPropSeachMonoMWREST(16.0313, 0.0001, 15)
    return results


assert len(test_methaneByMass()) < 3
assert type(test_methaneByMass()) is list
assert 291 in test_methaneByMass()


def test_alanineByMass():
    results = chemspiderREST.IntrinsicPropSeachMonoMWREST(89.0470, 0.0010, 15)
    return results


res = test_alanineByMass()
assert 234 in res
assert 1057 in res
assert 5735 in res
assert 582 in res
assert 387 in res
assert 96991 in res


def test_batchRecordGet():
    results = chemspiderREST.batchRecordsGet([2157, 236])
    return results


res = test_batchRecordGet()
assert len(res) == 2
assert len(res[0]) == 18
assert res[0]['smiles'] == 'c1ccccc1'
assert res[1]['inchiKey'] == 'BSYNRYMUTXBXSQ-UHFFFAOYAW'
assert res[1]['commonName'] == 'Aspirin'
assert res[1]['nominalMass'] == 180
